
# General
import os
import sys
import re
import numpy as np
import pandas as pd
import random
import scipy.constants as const
from scipy.stats import binned_statistic_2d
import math
import argparse
from pathlib import Path

# Dask
import dask.dataframe as dd

# Pyarrow
import pyarrow as pa
import pyarrow.parquet as pq

# hdf5
import h5py

# mpi4py
import mpi4py
from mpi4py import MPI

# OpenPMD
import openpmd_api as io
import openpmd_viewer as ov
from openpmd_viewer import OpenPMDTimeSeries
from openpmd_viewer.addons import LpaDiagnostics

# Load local packages
wdir=os.path.dirname(os.getcwd())
sys.path.append(wdir)

import src.dataManipulator as dm
from src.field_metainfo_dd import field_metainfo_dd
from src.openpmd_reader  import opmd_reader
from src.pplots          import phistogram_2d
from src.pplots          import phistogram_1d

def opmdfilter():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--opmd_dir",   "-d", type=str, help="OpenPMD input directory")
    parser.add_argument("--opmd_file",  "-f", type=str, help="OpenPMD selected file")    
    parser.add_argument("--output_dir", "-o", type=str, help="Analyzer output directory")
    parser.add_argument("--species",    "-s", type=str, default="electrons",
                        help="Particle species name (default: electrons)")

    args         = parser.parse_args()
    opmd_dir     = Path(args.opmd_dir)
    opmd_file    = args.opmd_file    
    output_dir   = Path(args.output_dir)    
    species_name = args.species

    # Get the structure of output opmd directory
    sim_names = []
    for root, dirs, files in os.walk(opmd_dir):
        for dir in dirs:
            if dir.endswith(".bp"):
                if not opmd_file:
                    #print(os.path.join(root, dir))
                    sim_names.append(dir)
                else:
                    if dir == opmd_file:
                        sim_names.append(dir)                        

    if not opmd_file:                    
        sim_names = sorted(sim_names, key=lambda x: int("".join([i for i in x if i.isdigit()])))           

    sim_base_path = str(opmd_dir) + '/'
    save_path = str(output_dir) + '/'

    
    # Get MPI setup
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    if 0 == comm.rank:
        print("Analyzer Running {} parallel MPI processes".format(size))    

    comm.Barrier() 
    t_start = MPI.Wtime()

    # Process each subdirs 
    for sim_name in sim_names:
        base_path = sim_base_path + sim_name        
        save_dir = save_path + sim_name + '/'

        
        if 0 == comm.rank:
            # Create the output directory for the current simulation if it doesn't exist
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)
            print("Analyzing: {}".format(base_path))
            print("Output: {}".format(save_dir))                    

        comm.Barrier()   
            
        # Create Reader   
        opmd_r = opmd_reader(sim_base_path, sim_name)
        series = opmd_r.get_series() 

        # Get time_step
        time_step = int(opmd_r.get_timestep_values()[0])
         
        # Get iteration for step=istep 
        s_istep = re.findall(r'\d+', sim_name)
        istep  = int(s_istep[0])
        i = series.iterations[istep]

        # Get Ex, Ez Fields info
        Ex, Ex_info, Ex_local = opmd_r.get_field(i, "E", "x")
        Ey, Ey_info, Ey_local = opmd_r.get_field(i, "E", "y")
        Ez, Ez_info, Ez_local = opmd_r.get_field(i, "E", "z")

        # Axis 
        x_axis = Ey_info.x
        y_axis = Ey_info.y        
        z_axis = Ey_info.z        

        nEx_nonzeros = opmd_r.get_nonzeros(Ex)
        nEy_nonzeros = opmd_r.get_nonzeros(Ey)
        nEz_nonzeros = opmd_r.get_nonzeros(Ez)        
        
        if 0 == comm.rank:         
            print(" x_axis: ", x_axis)
            print(" y_axis: ", y_axis)
            print(" z_axis: ", z_axis)            


        # Check the input files content
        # ( The first Warpx dump contains no E + paticle info)  
        if (nEx_nonzeros[0] == 0 and nEy_nonzeros[0] == 0 and nEz_nonzeros[0] == 0) :
            if 0 == comm.rank:         
                print("E arrays empty Ex {} Ey {} Ez {} for all ranks:  skipping analysis ..." \
                      .format(nEx_nonzeros[0], nEy_nonzeros[0], nEz_nonzeros[0]))
            # Skip and move to next simulation output
            continue
    
        # Some constants for normalisation    
        max_e_dens = 1e25
        threshold_energy = 0 # MeV
        wp = np.sqrt(max_e_dens*const.e**2/const.epsilon_0/const.m_e)
        field_normalization = const.e/const.m_e/const.c/wp
            
        # Get Particles Info 
        x_loc, y_loc, z_loc , px_loc, py_loc, pz_loc, w_loc = opmd_r.get_particles(i) 

        # Close here the input to free ressources 
        opmd_r.get_series().close()
        
        # Get XYZ boundaries
        x_min, x_max, y_min, y_max, z_min, z_max = opmd_r.get_xyz_limits(x_loc, y_loc, z_loc) 

        # Units in mum
        x_min*=1e6
        x_max*=1e6
        y_min*=1e6
        y_max*=1e6
        z_min*=1e6
        z_max*=1e6                
        
        # Zeros supression 
        x_loc  = x_loc[ np.where(x_loc != 0) ]
        y_loc  = y_loc[ np.where(y_loc != 0) ]
        z_loc  = z_loc[ np.where(z_loc != 0) ]
        px_loc = px_loc[ np.where(px_loc != 0) ]
        py_loc = py_loc[ np.where(py_loc != 0) ]
        pz_loc = pz_loc[ np.where(pz_loc != 0) ]
        w_loc  = w_loc[ np.where(w_loc!= 0) ]

        # Reduced momentum
        norm_factor = 1./(const.m_e * const.c) 
        px_loc = px_loc * norm_factor
        py_loc = py_loc * norm_factor
        pz_loc = pz_loc * norm_factor            

        # Reduce data with phys. selection
        # E selection
        threshold_energy = 1 # MeV            
        p_sqr = (px_loc)**2+(py_loc)**2+(pz_loc)**2
        E = const.m_e*const.c**2*np.sqrt(1+p_sqr)        
        e_inds = np.where(E/const.e/1e6>threshold_energy)[0]        
        E = E[e_inds]/const.e/1e6
        
        x_loc = x_loc[e_inds]*1e6
        y_loc = y_loc[e_inds]*1e6
        z_loc = z_loc[e_inds]*1e6    
        px_loc = px_loc[e_inds]
        py_loc = py_loc[e_inds]
        pz_loc = pz_loc[e_inds]    
        w_loc  = w_loc[e_inds]

        # Linearization Ex,y,z fields
        E_local_shape  = Ex_local.shape
        E_global_shape = Ex_info.shape
        
        # Use Ravel() -> more memory efficient than flatten() function !
        ex_data =  Ex_local.ravel(order='C')
        ey_data =  Ey_local.ravel(order='C')
        ez_data =  Ez_local.ravel(order='C')                

        # Combine the data into a single DataFrame
        combined_data_pd = pd.DataFrame({
            'ex': ex_data,
            'ey': ey_data,
            'ez': ez_data
        })

        # Convert to Dask DataFrame with only one partition / MPI rank
        combined_data_dd = dd.from_pandas(combined_data_pd, npartitions=1) 
        
        # Write a single Parquet file for all field data
        e_rank_filename = f"{save_dir}/fields_rank_{rank}.parquet"

        # Remove the existing file if it exists
        if os.path.exists(e_rank_filename):
            os.remove(e_rank_filename)
        
        # Use compression to optimize I/O
        combined_data_dd.to_parquet(e_rank_filename, engine='pyarrow', compression='snappy')
        
        # Write field meta info used: ( Ex )
        # Create an instance of FieldMetaInformation using the OpenPMD FieldMetaInfo
        Ex_info_dd = field_metainfo_dd(Ex_info, E_global_shape, E_local_shape)
        
        #if rank == 0:
        # Write to Parquet            
        Ex_info_dd.write_to_parquet(f"{save_dir}/e_meta_info_{rank}.parquet")
        #    Ex_info_dd.write_to_parquet(f"{save_dir}/e_meta_info.parquet")
            
        # Prepare particle data for Parquet output
        particle_data = {
            'x_loc': x_loc,
            'y_loc': y_loc,
            'z_loc': z_loc,
            'px_loc': px_loc,
            'py_loc': py_loc,
            'pz_loc': pz_loc,
            'w_loc': w_loc
        }

        # Create a Dask DataFrame for the particle data
        particle_df = dd.from_pandas(pd.DataFrame(particle_data), npartitions=1)

        # Write one Parquet file per rank for particle data
        rank_particle_filename = f"{save_dir}/particles_rank_{rank}.parquet"

        # Remove the existing file if it exists
        if os.path.exists(rank_particle_filename):
            os.remove(rank_particle_filename)

        particle_df.to_parquet(rank_particle_filename, engine='pyarrow')

        if rank == 0:
            print(f"E field data saved to: {e_rank_filename}")
            print(f"Particle data saved to: {rank_particle_filename}")
        comm.Barrier()
        t_end = MPI.Wtime()
        
        if rank == 0:
            print("Total time taken: {:.2f} seconds".format(t_end - t_start))
           
            
if __name__ == "__main__":
    opmdfilter()

