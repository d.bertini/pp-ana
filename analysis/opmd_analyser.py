
# General
import os
import sys
import re
import numpy as np
import pandas as pd
import random
import scipy.constants as const
from scipy.stats import binned_statistic_2d
import math
import argparse
from pathlib import Path

# hdf5
import h5py

# mpi4py
import mpi4py
from mpi4py import MPI

# OpenPMD
import openpmd_api as io
import openpmd_viewer as ov
from openpmd_viewer import OpenPMDTimeSeries
from openpmd_viewer.addons import LpaDiagnostics

# Load local packages
wdir=os.path.dirname(os.getcwd())
sys.path.append(wdir)

import src.dataManipulator as dm
from src.openpmd_reader  import opmd_reader
from src.pplots          import phistogram_2d
from src.pplots          import phistogram_1d

def analyze():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--opmd_dir",   "-d", type=str, help="OpenPMD input directory")
    parser.add_argument("--opmd_file",  "-f", type=str, help="OpenPMD selected file")    
    parser.add_argument("--output_dir", "-o", type=str, help="Analyzer output directory")
    parser.add_argument("--species",    "-s", type=str, default="electrons",
                        help="Particle species name (default: electrons)")

    args         = parser.parse_args()
    opmd_dir     = Path(args.opmd_dir)
    opmd_file    = args.opmd_file    
    output_dir   = Path(args.output_dir)    
    species_name = args.species

    # Get the structure of output opmd directory
    sim_names = []
    for root, dirs, files in os.walk(opmd_dir):
        for dir in dirs:
            if dir.endswith(".bp"):
                if not opmd_file:
                    #print(os.path.join(root, dir))
                    sim_names.append(dir)
                else:
                    if dir == opmd_file:
                        sim_names.append(dir)                        

    if not opmd_file:                    
        sim_names = sorted(sim_names, key=lambda x: int("".join([i for i in x if i.isdigit()])))           

    sim_base_path = str(opmd_dir) + '/'
    save_path = str(output_dir) + '/'

    
    # Get MPI setup
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    if 0 == comm.rank:
        print("Analyzer Running {} parallel MPI processes".format(size))    

    comm.Barrier() 
    t_start = MPI.Wtime()

    # Process each subdirs 
    for sim_name in sim_names:
        base_path = sim_base_path + sim_name        
        save_dir = save_path + sim_name + '/'
        
        if 0 == comm.rank:
            print("Analyzing: {}".format(base_path))
            print("Output: {}".format(save_dir))                    
            
        # Create Reader   
        opmd_r = opmd_reader(sim_base_path, sim_name)
        series = opmd_r.get_series() 

        # Get time_step
        time_step = int(opmd_r.get_timestep_values()[0])
         
        # Get iteration for step=istep 
        s_istep = re.findall(r'\d+', sim_name)
        istep  = int(s_istep[0])
        i = series.iterations[istep]

        # Get Ex, Ez Fields info
        Ex, Ex_info = opmd_r.get_field(i, "E", "x")
        Ey, Ey_info = opmd_r.get_field(i, "E", "y")
        Ez, Ez_info = opmd_r.get_field(i, "E", "z")

        # Axis 
        x_axis = Ey_info.x
        y_axis = Ey_info.y        
        z_axis = Ey_info.z        

        nEx_nonzeros = opmd_r.get_nonzeros(Ex)
        nEy_nonzeros = opmd_r.get_nonzeros(Ey)
        nEz_nonzeros = opmd_r.get_nonzeros(Ez)        
        
        if 0 == comm.rank:         
            print(" x_axis: ", x_axis)
            print(" y_axis: ", y_axis)
            print(" z_axis: ", z_axis)            


        # Check the input files content
        # ( The first Warpx dump contains no E + paticle info)  
        if (nEx_nonzeros[0] == 0 and nEy_nonzeros[0] == 0 and nEz_nonzeros[0] == 0) :
            if 0 == comm.rank:         
                print("E arrays empty Ex {} Ey {} Ez {} for all ranks:  skipping analysis ..." \
                      .format(nEx_nonzeros[0], nEy_nonzeros[0], nEz_nonzeros[0]))
            # Skip and move to next simulation output
            continue
    
        # Some constants for normalisation    
        max_e_dens = 1e25
        threshold_energy = 0 # MeV
        wp = np.sqrt(max_e_dens*const.e**2/const.epsilon_0/const.m_e)
        field_normalization = const.e/const.m_e/const.c/wp
            
        # Get Particles Info 
        x_loc, y_loc, z_loc , px_loc, py_loc, pz_loc, w_loc = opmd_r.get_particles(i) 

        # Close here the input to free ressources 
        opmd_r.get_series().close()
        
        # Get XYZ boundaries
        x_min, x_max, y_min, y_max, z_min, z_max = opmd_r.get_xyz_limits(x_loc, y_loc, z_loc) 

        # Units in mum
        x_min*=1e6
        x_max*=1e6
        y_min*=1e6
        y_max*=1e6
        z_min*=1e6
        z_max*=1e6                
        
        # Zeros supression 
        x_loc  = x_loc[ np.where(x_loc != 0) ]
        y_loc  = y_loc[ np.where(y_loc != 0) ]
        z_loc  = z_loc[ np.where(z_loc != 0) ]
        px_loc = px_loc[ np.where(px_loc != 0) ]
        py_loc = py_loc[ np.where(py_loc != 0) ]
        pz_loc = pz_loc[ np.where(pz_loc != 0) ]
        w_loc  = w_loc[ np.where(w_loc!= 0) ]

        # Reduced momentum
        norm_factor = 1./(const.m_e * const.c) 
        px_loc = px_loc * norm_factor
        py_loc = py_loc * norm_factor
        pz_loc = pz_loc * norm_factor            

        # Reduce data with phys. selection
        # E selection
        threshold_energy = 1 # MeV            
        p_sqr = (px_loc)**2+(py_loc)**2+(pz_loc)**2
        E = const.m_e*const.c**2*np.sqrt(1+p_sqr)        
        e_inds = np.where(E/const.e/1e6>threshold_energy)[0]        
        E = E[e_inds]/const.e/1e6
        
        x_loc = x_loc[e_inds]*1e6
        y_loc = y_loc[e_inds]*1e6
        z_loc = z_loc[e_inds]*1e6    
        px_loc = px_loc[e_inds]
        py_loc = py_loc[e_inds]
        pz_loc = pz_loc[e_inds]    
        w_loc  = w_loc[e_inds]
        
        # Electron energy
        nz_bins=500
        nx_bins=200

        # Electron energy
        phist_2d = phistogram_2d(z_loc, x_loc, bins = [nz_bins, nx_bins], \
                                 weights = E, \
                                 range=[[z_min,z_max],[x_min,x_max]])
        
        H_n, x_bounds, y_bounds = phist_2d.book_histogram()  

          
        
        # Electron spectrum using 2 methods
        # Method 1: booking a parallel 1D histogram 
        e_min, e_max = opmd_r.get_limits(E)

        print(" rank {} E size {} E_min {} E_max {}".format(rank, E.size, e_min, e_max))
        n_bins=2048
        phist_1d = phistogram_1d(E, bins = n_bins, weights = w_loc,  \
                                 range = [e_min, e_max], normed=False)
        
        e_hist, e_bins = phist_1d.book_histogram()
        e_bincenter = (e_bins[:-1] + e_bins[1:]) * 0.5

        # Method 2: gathering full E,W arrays
        e_global, e_global_size = opmd_r.gather_array(i, E)
        w_global, w_global_size = opmd_r.gather_array(i, w_loc)        
        
        # Add JH code here for testing purpose
        div_x_plane = np.arctan2(px_loc, pz_loc)*180/np.pi

        div_x_min, div_x_max = opmd_r.get_limits(div_x_plane)

        print(" rank {} div_x_plane size {} min {} max {}".format(rank, div_x_plane.size, div_x_min, div_x_max))                        
        n_bins=2048
        phist_div_x_1d = phistogram_1d(div_x_plane, bins = n_bins, weights = w_loc,  \
                                 range = [div_x_min, div_x_max], normed=False)
        
        div_x_hist, div_x_bins = phist_div_x_1d.book_histogram()
        div_x_bincenter = (div_x_bins[:-1] + div_x_bins[1:]) * 0.5
        div_x_width = dm.width(div_x_hist, axis = div_x_bincenter, factor = 0.5)

        # y-z-plane
        div_y_plane = np.arctan2(py_loc, pz_loc)*180/np.pi

        div_y_min, div_y_max = opmd_r.get_limits(div_y_plane)

        print(" rank {} div_y_plane size {} min {} max {}".format(rank, div_y_plane.size, div_y_min, div_y_max))                        
        n_bins=2048
        phist_div_y_1d = phistogram_1d(div_y_plane, bins = n_bins, weights = w_loc,  \
                                 range = [div_y_min, div_y_max], normed=False)
        
        div_y_hist, div_y_bins = phist_div_y_1d.book_histogram()
        div_y_bincenter = (div_y_bins[:-1] + div_y_bins[1:]) * 0.5
        div_y_width = dm.width(div_y_hist, axis = div_y_bincenter, factor = 0.5)

        comm.Barrier()
        t_diff = MPI.Wtime()-t_start
        
        if 0 == comm.rank:
            # Viz. packages 
            import matplotlib
            matplotlib.use('Agg')
            import matplotlib.pyplot as plt
            plt.switch_backend('agg')
            from matplotlib.colors import LogNorm
            import matplotlib.cm as cm
            from matplotlib.backends.backend_agg import FigureCanvas
            
            # Electron energy
            if not os.path.isdir(save_dir+'e/e_energy/'):
                os.makedirs(save_dir+'e/e_energy/')    
            
            plt.imshow(H_n.T, origin='lower',  cmap='seismic',
                       extent=[z_min, z_max, x_min, x_max])
            fig = plt.gcf()
            fig.set_size_inches(19, 4)
            plt.title('e Energy')
            plt.xlabel('z in µm')
            plt.ylabel('x in µm')            
            cb = plt.colorbar()
            cb.set_label('Energy in MeV')
            plt.savefig(save_dir+'e/e_energy/'+'electron_energy_'+str(istep)+'.png', dpi=100)          
            plt.close()
            print('Electron energy histogram saved!')

        
            # Method 2
            print(e_hist)
            print(e_bincenter)
            plt.semilogy(e_bincenter,e_hist)
            plt.title('electron spectrum: '+str(np.round(e_max,2))+'MeV')
            plt.xlabel('electron energy in MeV')
            plt.savefig(save_dir+'e/e_energy/'+'electron_spectrum2_'+str(istep)+'.png', dpi=100)
            plt.close()
            print('Electron spectrum histogram2 saved!')            
            
                
            # Method 1
            energy_hist, nbins = np.histogram(e_global, bins=2048, weights=w_global)
            bincenter = (nbins[:-1] + nbins[1:]) * 0.5
            max_e = bincenter[-1]
            plt.semilogy(bincenter,energy_hist)#
            plt.title('electron spectrum: '+str(np.round(max_e,2))+'MeV')
            plt.xlabel('electron energy in MeV')
            plt.savefig(save_dir+'e/e_energy/'+'electron_spectrum_'+str(istep)+'.png', dpi=100)
            plt.close()

            ### Laser fields (Ex and Ey)
            if not os.path.isdir(save_dir+'E-field/Ey/'):
                os.makedirs(save_dir+'E-field/Ey/')
            #Ey_new = Ey[:,:,len(y_axis)//2]
            Ey_new = Ey
            print("Ey dim: {} Ey_new dim: {}".format(Ey.ndim, Ey_new.ndim))            
            plt.imshow(Ey_new.T*field_normalization, \
                       extent=[z_axis[0]*1e6, \
                               z_axis[-1]*1e6, \
                               (y_axis[0])*1e6, \
                               (y_axis[-1])*1e6], \
                       aspect='equal')                       


            fig = plt.gcf()
            fig.set_size_inches(19, 4)
            plt.colorbar(label='electric field in V/m')
            plt.title('Laser field, t='+str(np.round(Ex_info.time*1e12,2))+' ps (time step: '+str(istep)+')')
            plt.xlabel('z in µm')
            plt.xlabel('x in µm')
            plt.savefig(save_dir+'E-field/Ey/'+'Ey_'+str(istep)+'.png')
            plt.close()

            # Electron divergence

            if not os.path.isdir(save_dir+'e/e_div/'):
                os.makedirs(save_dir+'e/e_div/')

            plt.plot(div_y_bincenter,div_y_hist)
            plt.title('electron divergence y-z plane (FWHM): '+str(np.round(div_y_width,2))+'°')
            plt.xlabel('angle in °')
            plt.savefig(save_dir+'e/e_div/'+'electron_divergence_y-z_histogram_'+str(istep)+'.png', dpi=100)
            np.savetxt(save_dir+'e/e_energy/'+'electron_spectrum_E_'+str(istep)+'.dat', div_y_bincenter)
            np.savetxt(save_dir+'e/e_energy/'+'electron_spectrum_dN_dE_'+str(istep)+'.dat', div_y_hist)
            plt.close()

            plt.plot(div_x_bincenter,div_x_hist)
            plt.title('electron divergence x-z plane (FWHM): '+str(np.round(div_x_width,2))+'°')
            plt.xlabel('angle in °')
            plt.savefig(save_dir+'e/e_div/'+'electron_divergence_x-z_histogram_'+str(istep)+'.png', dpi=100)
            np.savetxt(save_dir+'e/e_energy/'+'electron_spectrum_E_'+str(istep)+'.dat', div_x_bincenter)
            np.savetxt(save_dir+'e/e_energy/'+'electron_spectrum_dN_dE_'+str(istep)+'.dat', div_x_hist)
            plt.close()
            print('Electron divergence saved!')
            
            
if __name__ == "__main__":
    analyze()

