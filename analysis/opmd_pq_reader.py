# General
from mpi4py import MPI
import numpy as np
import os
import sys
import re
import numpy as np
import pandas as pd
import random
import scipy.constants as const
from scipy.stats import binned_statistic_2d
import math
import argparse
from pathlib import Path

# Dask
import dask.dataframe as dd
from dask.distributed import Client

# OpenPMD
import openpmd_api as io
import openpmd_viewer as ov
from openpmd_viewer import OpenPMDTimeSeries
from openpmd_viewer.addons import LpaDiagnostics


# Load local packages
wdir=os.path.dirname(os.getcwd())
sys.path.append(wdir)

import src.dataManipulator as dm
from src.openpmd_reader     import opmd_reader
from src.pplots             import phistogram_2d
from src.pplots             import phistogram_1d
from src.opmd_meta_data     import opmd_meta_data, load_metadata
from src.field_analyzer     import FieldAnalyzer
from src.particle_analyzer  import ParticleAnalyzer
    
def analyze():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--pq_dir",     "-d", type=str, help="Parquet input directory")
    parser.add_argument("--output_dir", "-o", type=str, help="Plots output directory")
    parser.add_argument("--opmd_file",  "-f", type=str, help="OpenPMD selected file")    
    parser.add_argument("--species",    "-s", type=str, default="electrons",
                        help="Particle species name (default: electrons)")
    parser.add_argument("--analyze",    "-a", type=str, choices=['field', 'particle', 'full'], default='full',
                        help="Specify which analysis to run: 'field', 'particle', or 'full' (default: full)")
    
    args = parser.parse_args()
    opmd_file          = args.opmd_file    
    input_pq_dir       = args.pq_dir + '/' + opmd_file + '/'
    output_plt_dir   = args.output_dir + '/' + opmd_file + '/'
    
    # Initialize the FieldAnalyzer if specified
    if args.analyze in ['field', 'full']:
        f_analyzer = FieldAnalyzer(input_pq_dir, output_plt_dir)

    # Initialize the ParticleAnalyzer if specified
    if args.analyze in ['particle', 'full']:
        p_analyzer = ParticleAnalyzer(input_pq_dir, output_plt_dir, args.species)

    # Particles analysis
    if args.analyze in ['particle', 'full']:
        p_analyzer.comm.Barrier()
        t_start = MPI.Wtime()

        # Read particle data
        p_analyzer.read_particle_data()
        
        # Synchronize all ranks to ensure data is ready
        p_analyzer.comm.Barrier()

        # Extract particle locations
        part_data = p_analyzer.extract_particle_data()
        if part_data is None:
            return  # Exit if there was an error extracting particle_data

        x_loc, y_loc, z_loc, px_loc, py_loc, pz_loc, w_loc = part_data

        # Calculate energy
        energy = p_analyzer.calculate_energy(px_loc, py_loc, pz_loc)

        # Get XYZ boundaries
        x_min, x_max, y_min, y_max, z_min, z_max = p_analyzer.get_xyz_limits(x_loc, y_loc, z_loc)

        # Define histogram parameters
        nz_bins = 500
        nx_bins = 200

        # Create and book the 2D histogram
        d_range = [[z_min, z_max], [x_min, x_max]]
        H_n, x_bounds, y_bounds = p_analyzer.create_2d_histogram(z_loc, x_loc, weights=energy, bins=[nz_bins, nx_bins], data_range=d_range)

        # Save the histogram
        p_analyzer.save_histogram(H_n, z_min, z_max, x_min, x_max)

        # Analyze divergence
        p_analyzer.analyze_divergence(px_loc, py_loc, pz_loc, w_loc)

        # Elapsed Time measurement
        p_analyzer.comm.Barrier()
        t_end = MPI.Wtime()
        
        if p_analyzer.rank == 0:
            print("Total time taken: {:.2f} seconds".format(t_end - t_start))

    # Fields analysis
    if args.analyze in ['field', 'full']:
        f_analyzer.read_metadata()
        f_analyzer.read_fields()

        # Read  E fields from file 
        Ey, Ey_info, Ey_local = f_analyzer.extract_field_data('ey')
        
        # Plot the electric field data
        f_analyzer.plot_field_data(field_type='E', component='y', field_data=Ey, field_info=Ey_info)
        

if __name__ == "__main__":
    analyze()

