import os
import numpy as np
import numbers
import pandas as pd
from mpi4py import MPI
import openpmd_api as io
from abc import ABC, abstractmethod
from typing import Callable, Tuple

# Declare global MPI setup
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()


class phistogram (ABC):
    """Parallel histogram base"""
    def __init__(self, dim):
        self.dim = dim

    @abstractmethod
    def book_histogram(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """
        Abstract method. book histograms
        """


class  phistogram_1d(phistogram):
    """
    Parallel Compute a 1D histogram with equally space bins 
    
    Parameters
    ----------
       x     :  type: np.ndarray
                The position of the points to bin in the 2D histogram
       bins  :  type: int 
                The number of bins.

       range :  type: iterable
                The range to use in each dimention, as tuple
                (xmin, xmax)
       weights : type: np..ndarray
                 The weights of the points in the 1D histogram

       Returns
       -------
       array : type np.ndarray
               The 1D histogram array
    """

    def __init__(self, x, bins=None, range=None, normed=False, weights=None):
        super().__init__(1)
        self.x = np.atleast_1d(x)
        self.normed=normed
        self.weights=weights
        # Do some sanity checks
        if not np.isscalar(bins):
            raise TypeError("bins must be an integer")
     
        self.nx =  bins
        self.xmin, self.xmax = range
        # Do some sanity checks
        if not np.isfinite(self.xmin):
            raise ValueError("xmin notfinite")
        if not np.isfinite(self.xmax):
            raise ValueError("xmax not finite")
        if self.xmax <= self.xmin:
            raise ValueError("xmax > xmin")
        if self.nx <= 0:
            raise ValueError("nx > 0 ")
        
    def book_histogram(self):
        #print(" x size: {} h_dim: {}".format(self.x.size, self.dim))

        data_list = []
        x_bins = np.linspace(self.xmin, self.xmax, self.nx+1)
          
        # Create duplet numpy histograms   
        H, xedges = np.histogram(self.x, bins = x_bins, \
                                           weights = self.weights, \
                                           range=[[self.xmin,self.xmax]])
        
        H_counts, xedges = np.histogram(self.x, \
                                                  bins = x_bins, \
                                                  range=[[self.xmin,self.xmax]])

        H = np.ascontiguousarray(H)
        H_counts = np.ascontiguousarray(H_counts)                

        # Send partial_hist all ranks 
        dest1 = np.zeros_like(H)
        comm.Reduce(
            [H, MPI.DOUBLE],
            [dest1, MPI.DOUBLE],
            op=MPI.SUM,
            root=0
        )

        dest2 = np.ones_like(H_counts)
        comm.Reduce(
            [H_counts, MPI.DOUBLE],
            [dest2, MPI.DOUBLE],
            op=MPI.SUM,
            root=0
        )        

        # normalize if needed        
        if self.normed:
            h_result = np.divide(dest1, dest2, where=(dest2 != 0), out=np.zeros_like(dest1))
#            h_result = dest1/dest2
        else:
            h_result = dest1
            
        data_list.append(h_result)
        data_list.append(xedges)
        
        return(data_list)
        

class  phistogram_2d(phistogram):
    """
    Parallel Compute a 2D histogram 
    
    Parameters
    ----------
       x, y  :  type: np.ndarray
                The position of the points to bin in the 2D histogram
       bins  :  type: int or iterable
                The number of bins in each dimension. If given as an integer, the same
                number of bins is used for each dimension.
       range :  type: iterable
                The range to use in each dimention, as an iterable of value pairs, i.e.
                [(xmin, xmax), (ymin, ymax)]
       weights : type: np..ndarray
                 The weights of the points in the 2D histogram

       Returns
       -------
       array : type np.ndarray
               The 2D histogram array
    """

    def __init__(self, x, y, bins=None, range=None, normed=True, weights=None):
        super().__init__(2)
        self.x = np.atleast_1d(x)
        self.y = np.atleast_1d(y)
        self.normed=normed
        self.weights=weights

        if isinstance(bins, numbers.Integral):
            self.nx = self.ny = bins
        else:
            self.nx, self.ny = bins

        (self.xmin, self.xmax), (self.ymin, self.ymax) = range

        # Do some sanity checks
        if self.xmax <= self.xmin:
            raise ValueError("need: xmax > xmin")
        
        if self.ymax <= self.ymin:
            raise ValueError("nedd: ymax > ymin")
        
        if self.nx <= 0:
            raise ValueError("need: nx > 0 ")
        
        if self.ny <= 0:
            raise ValueError("need: ny > 0 ")

        
    def book_histogram(self):
        #print("book_histogram")    
        #print(" x size: {} h_dim: {}".format(self.x.size, self.dim))

        data_list = []
        x_bins = np.linspace(self.xmin, self.xmax, self.nx+1)
        y_bins = np.linspace(self.ymin, self.ymax, self.ny+1)

        # Create duplet numpy histograms   
        H, xedges, yedges = np.histogram2d(self.x, self.y, bins = [x_bins, y_bins], \
                                           weights = self.weights, \
                                           range=[[self.xmin,self.xmax],[self.ymin,self.ymax]])
        
        H_counts, xedges, yedges = np.histogram2d(self.x, self.y, \
                                                  bins = [x_bins, y_bins], \
                                                  range=[[self.xmin,self.xmax],[self.ymin,self.ymax]])

        H = np.ascontiguousarray(H)
        H_counts = np.ascontiguousarray(H_counts)                
                      
        # Send partial_hist all ranks 
        dest1 = np.zeros_like(H)
        dest2 = np.ones_like(H_counts)

        comm.Reduce(
            [H, MPI.DOUBLE],
            [dest1, MPI.DOUBLE],
            op=MPI.SUM,
            root=0
        )

        comm.Reduce(
            [H_counts, MPI.DOUBLE],
            [dest2, MPI.DOUBLE],
            op=MPI.SUM,
            root=0
        )        

        # normalize
        h_result = np.divide(dest1, dest2, where=(dest2 != 0), out=np.zeros_like(dest1))
        #h_result = dest1/dest2
        
        data_list.append(h_result)
        data_list.append(xedges)
        data_list.append(yedges)
        
        return(data_list)
