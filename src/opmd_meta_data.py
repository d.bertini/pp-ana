from dataclasses import dataclass
import dask.dataframe as dd

@dataclass
class opmd_meta_data:
    """
    meta_data class  containing global information about the simulation.
    For the moment only fields related  

    Attributes:
        x_min (float): Minimum x-coordinate.
        x_max (float): Maximum x-coordinate.
        z_min (float): Minimum z-coordinate.
        z_max (float): Maximum z-coordinate.
        dx (float): Change in x (grid spacing in x-direction).
        dz (float): Change in z (grid spacing in z-direction).
        time (float): Time value.
        gdim_x (int): Global Dimension size in x-direction.
        gdim_y (int): Global Dimension size in y-direction.
        gdim_z (int): Global Dimension size in z-direction.
        ldim_x (int): Local Dimension size in x-direction.
        ldim_y (int): Local Dimension size in y-direction.
        ldim_z (int): Local Dimension size in z-direction.
        field_attrs (dict): Attributes related to the field (e.g., metadata).
        component_attrs (dict): Attributes related to components (e.g., metadata).
    """
    x_min: float
    x_max: float
    z_min: float
    z_max: float
    dx: float
    dz: float
    time: float
    gdim_x: int
    gdim_y: int
    gdim_z: int
    ldim_x: int
    ldim_y: int
    ldim_z: int    
    field_attrs: dict
    component_attrs: dict

def load_metadata(local_meta_data: dd.DataFrame) -> opmd_meta_data:
    """
    Load metadata from a Dask DataFrame.
    Args:
        local_meta_data (dd.DataFrame): A Dask DataFrame containing metadata.
    Returns:
        opmd_meta_data: An instance of the opmd_meta_data class populated with values 
        from the DataFrame.
    Raises:
        ValueError: If the DataFrame is empty or does not contain the 
        expected columns.
    """
    
    # Compute the Dask DataFrame to get the actual values as a Pandas DataFrame
    computed_data = local_meta_data.compute()
    
    # Check if the DataFrame is empty or does not contain the expected columns
    if computed_data.empty or not all(
        col in computed_data.columns for col in [
            'xmin', 'xmax', 'zmin', 'zmax', 'dx', 'dz', 
            't', 'g_shape_x', 'g_shape_y', 'g_shape_z',
            'l_shape_x', 'l_shape_y', 'l_shape_z', 
            'field_attrs', 'component_attrs'
        ]
    ):
        raise ValueError("The DataFrame is empty or does not contain the expected columns.")


    return opmd_meta_data(
        x_min=computed_data['xmin'].values[0],  
        x_max=computed_data['xmax'].values[0],  
        z_min=computed_data['zmin'].values[0],  
        z_max=computed_data['zmax'].values[0],  
        dx=computed_data['dx'].values[0],       
        dz=computed_data['dz'].values[0],       
        time=computed_data['t'].values[0],      
        gdim_x=int(computed_data['g_shape_x'].values[0]),  
        gdim_y=int(computed_data['g_shape_y'].values[0]),  
        gdim_z=int(computed_data['g_shape_z'].values[0]),
        ldim_x=int(computed_data['l_shape_x'].values[0]),  
        ldim_y=int(computed_data['l_shape_y'].values[0]),  
        ldim_z=int(computed_data['l_shape_z'].values[0]),                
        field_attrs=computed_data['field_attrs'].values[0],  
        component_attrs=computed_data['component_attrs'].values[0]  
    )



