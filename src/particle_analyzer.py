# Particle Analyzer class

from mpi4py import MPI
import numpy as np
import os
import sys
import scipy.constants as const
import dask.dataframe as dd
from pathlib import Path
from src.pplots import phistogram_2d, phistogram_1d  
import src.dataManipulator as dm  

        
class ParticleAnalyzer:
    """
    A class to analyze particle data from parquet files in a parallel computing environment using MPI.

    Attributes:
    ----------
    pq_dir : Path
        The directory containing the parquet files for particle data.
    output_dir : Path
        The directory where output plots and histograms will be saved.
    species_name : str
        The name of the particle species being analyzed.
    comm : MPI.Comm
        The MPI communicator for parallel processing.
    rank : int
        The rank of the current process in the MPI environment.
    size : int
        The total number of processes in the MPI environment.
    local_part_data : DataFrame
        Pandas DataFrame containing computed local particle data.

    Methods:
    -------
    read_particle_data():
        Reads particle data from a parquet file and stores it in local attributes.
    
    extract_particle_data():
        Extracts position and momentum data from the local particle DataFrame.
    
    calculate_energy(px_loc, py_loc, pz_loc):
        Calculates the energy of particles based on their momentum components.
    
    get_xyz_limits(x_loc, y_loc, z_loc):
        Gathers the minimum and maximum boundaries of particle positions across all MPI processes.
    
    create_2d_histogram(z_loc, x_loc, weights, bins, data_range):
        Creates a 2D histogram of particle data.
    
    create_1d_histogram(data, weights, bins, data_range):
        Creates a 1D histogram of particle data.
    
    save_histogram(H_n, x_min, x_max, y_min, y_max):
        Saves a 2D histogram plot to the output directory.
    
    save_1d_histogram(bin_centers, hist, title, xlabel, filename):
        Saves a 1D histogram plot to the output directory.
    
    get_limits(x_loc):
        Gathers the minimum and maximum values of a given array across all MPI processes.
    
    analyze_divergence(px_loc, py_loc, pz_loc, w_loc):
        Analyzes the divergence angles of particles and saves the corresponding histograms.
    """

    def __init__(self, pq_dir, output_dir, species_name):
        self.pq_dir = Path(pq_dir)
        self.output_dir = Path(output_dir)
        self.species_name = species_name
        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()
        self.size = self.comm.Get_size()
        self.local_part_data = None
        
                
    def read_particle_data(self):
        rank_particle_file = f"{self.pq_dir}/particles_rank_{self.rank}.parquet"
        part_df = dd.read_parquet(rank_particle_file, engine='pyarrow')
        self.local_part_data = part_df.compute()

        if self.local_part_data.empty:
            print(f"read_particle_data() at rank: {self.rank} -> The DataFrame is empty.")
        else:
            print(f"read_particle_data() at rank {self.rank} -> The DataFrame has {len(self.local_part_data)} rows.")
             

    def extract_particle_data(self):
        try:
            x_loc = self.local_part_data['x_loc'].values
            y_loc = self.local_part_data['y_loc'].values
            z_loc = self.local_part_data['z_loc'].values
            px_loc = self.local_part_data['px_loc'].values
            py_loc = self.local_part_data['py_loc'].values
            pz_loc = self.local_part_data['pz_loc'].values
            w_loc = self.local_part_data['w_loc'].values
            return x_loc, y_loc, z_loc, px_loc, py_loc, pz_loc, w_loc
        except KeyError as e:
            print(f"extract_particle_data() at rank {self.rank}: KeyError - {e}")
            return None

    def calculate_energy(self, px_loc, py_loc, pz_loc):
        p_sqr = (px_loc)**2 + (py_loc)**2 + (pz_loc)**2
        return const.m_e * const.c**2 * np.sqrt(1 + p_sqr)

    def get_xyz_limits(self, x_loc, y_loc, z_loc):
        #
        # Gather XYZ boundaries information
        #
        data_list = []
        
        # Initialize the np arrays that will store the results:
        x_min = np.array([np.inf], 'd')
        x_max = np.array([-np.inf], 'd')
        y_min = np.array([np.inf], 'd')
        y_max = np.array([-np.inf], 'd')
        z_min = np.array([np.inf], 'd')
        z_max = np.array([-np.inf], 'd')
        
        # Check if the input arrays are empty
        if len(x_loc) > 0:
            x_min[0] = np.min(x_loc)
            x_max[0] = np.max(x_loc)
        if len(y_loc) > 0:
            y_min[0] = np.min(y_loc)
            y_max[0] = np.max(y_loc)
        if len(z_loc) > 0:
            z_min[0] = np.min(z_loc)
            z_max[0] = np.max(z_loc)
            
        # Perform the reductions:
        self.comm.Allreduce(x_min, x_min, op=MPI.MIN)
        self.comm.Allreduce(x_max, x_max, op=MPI.MAX)
        self.comm.Allreduce(y_min, y_min, op=MPI.MIN)
        self.comm.Allreduce(y_max, y_max, op=MPI.MAX)
        self.comm.Allreduce(z_min, z_min, op=MPI.MIN)
        self.comm.Allreduce(z_max, z_max, op=MPI.MAX)
        
        data_list.append(x_min[0])
        data_list.append(x_max[0])
        data_list.append(y_min[0])
        data_list.append(y_max[0])
        data_list.append(z_min[0])
        data_list.append(z_max[0])

        return data_list

    def create_2d_histogram(self, z_loc, x_loc,  weights, bins, data_range): 
        phist_2d = phistogram_2d(z_loc, x_loc, bins=bins, weights=weights, range=data_range)
        return phist_2d.book_histogram()


    def create_1d_histogram(self, data, weights, bins, data_range):
        phist_1d = phistogram_1d(data, bins=bins, weights=weights, range=data_range)
        return phist_1d.book_histogram()

    
    def save_histogram(self, H_n, x_min, x_max, y_min, y_max):
        if self.rank == 0:
            # Visualization
            import matplotlib
            matplotlib.use('Agg')  # Use a non-interactive backend
            import matplotlib.pyplot as plt
            from matplotlib.colors import LogNorm

            save_dir = self.output_dir / "e/e_energy/"
            os.makedirs(save_dir, exist_ok=True)
            plt.imshow(H_n.T, origin='lower', cmap='seismic',
                       extent=[x_min, x_max, y_min, y_max], norm=LogNorm())
            fig = plt.gcf()
            fig.set_size_inches(19, 4)
            cb = plt.colorbar()
            cb.set_label('Energy in MeV')
            plt.title('Electron Energy Histogram')
            plt.xlabel('z in µm')
            plt.ylabel('x in µm')
            plt.savefig(os.path.join(save_dir, 'electron_energy.png'), dpi=100)
            plt.close()
            print('Electron energy histogram saved!')

    def save_1d_histogram(self, bin_centers, hist, title, xlabel, filename):
        if self.rank == 0:
            import matplotlib.pyplot as plt
            plt.semilogy(bin_centers, hist)
            plt.title(title)
            plt.xlabel(xlabel)
            plt.savefig(filename, dpi=100)
            plt.close()
            print(f'{title} saved!')


    def get_limits(self,x_loc):
        #
        # Gather array boundaries information
        #
        data_list = []
        
        # Initialize the np arrays that will store the results:
        #x_min      = np.array([np.inf], dtype='d')
        #x_max      = np.array([-np.inf], dtype='d')
        x_min      = np.array(0.0,'d')
        x_max      = np.array(0.0,'d')
        
        # CHECK ME !!
        x_loc_max = np.max(x_loc,initial=-1*sys.float_info.max)
        x_loc_min = np.min(x_loc,initial=+1*sys.float_info.max)

        print("rank: {} local_max: {} local_min: {}".format(self.rank, x_loc_max, x_loc_min))
        # perform the reductions:
        self.comm.Allreduce(x_loc_min, x_min, op=MPI.MIN)
        self.comm.Allreduce(x_loc_max, x_max, op=MPI.MAX)
        print("rank: {} x_max: {} x_min: {}".format(self.rank, x_max, x_min))        

        data_list.append(x_min)
        data_list.append(x_max)
        return(data_list)

            
    def analyze_divergence(self, px_loc, py_loc, pz_loc, w_loc):
        # Calculate divergence angles
        div_x_plane = np.arctan2(px_loc, pz_loc) * 180 / np.pi
        div_y_plane = np.arctan2(py_loc, pz_loc) * 180 / np.pi

        div_x_min, div_x_max = self.get_limits(div_x_plane)
        div_y_min, div_y_max = self.get_limits(div_y_plane)        

        # Check if the global min/max are still inf/-inf
        #if div_x_min == np.inf or div_x_max == -np.inf:
        #    print(f"Rank {self.rank}: div_x_plane is empty. Using default values.")
        #    div_x_min, div_x_max = -180, 180  # Set default values
            
        #if div_y_min == np.inf or div_y_max == -np.inf:
        #    print(f"Rank {self.rank}: div_y_plane is empty. Using default values.")
        #    div_y_min, div_y_max = -180, 180  # Set default values

        n_bins = 2048
        div_x_hist, div_x_bins = self.create_1d_histogram(div_x_plane, w_loc, n_bins, [div_x_min, div_x_max])
        div_y_hist, div_y_bins = self.create_1d_histogram(div_y_plane, w_loc, n_bins, [div_y_min, div_y_max])
        
        div_x_bincenter = (div_x_bins[:-1] + div_x_bins[1:]) * 0.5
        div_y_bincenter = (div_y_bins[:-1] + div_y_bins[1:]) * 0.5
        
        div_x_width = dm.width(div_x_hist, axis=div_x_bincenter, factor=0.5)
        div_y_width = dm.width(div_y_hist, axis=div_y_bincenter, factor=0.5)
        
        save_dir = self.output_dir / "e/e_div/"
        os.makedirs(save_dir, exist_ok=True)
        
        self.save_1d_histogram(
            div_x_bincenter,
            div_x_hist,
            f'Electron Divergence x-z Plane (FWHM: {div_x_width:.2f}°)',
            'Angle in °',
            os.path.join(save_dir, 'electron_divergence_x-z_histogram.png')
        )
        
        self.save_1d_histogram(
            div_y_bincenter,
            div_y_hist,
            f'Electron Divergence y-z Plane (FWHM: {div_y_width:.2f}°)',
            'Angle in °',
            os.path.join(save_dir, 'electron_divergence_y-z_histogram.png')
        )

