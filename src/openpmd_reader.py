
import os
import sys
import numpy as np
import pandas as pd
from mpi4py import MPI
import openpmd_api as io

from src.field_metainfo import FieldMetaInformation

# Declare global MPI setup
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

class opmd_reader ( object ):
    """A reader that reads openPMD format """
    def __init__(self, filename, pattern="openpmd_%06T.bp"):
        self._filename = filename
        self._series = None  # openPMD-api's data series holder
        self._iterations = None 
        self._timemap = {}  # maps time steps (int) in _series to physical time (float)
        self._timevalues = None  # the float values in _timemap 
        self._fields = None
        self._particles = None
        self._species = None
 
        if not self._series:
            self._series = io.Series(
                os.path.dirname(self._filename) + "/" + pattern,
                io.Access_Type.read_only,
                comm
            )
        iterations = []    
        timevalues = []
        fields = []
        particles = []
        species = []
        need_update=True

        iterations = np.array( self._series.iterations )
        
        for idx, iteration in self._series.iterations.items():
            # time
            if callable(iteration.time): 
                time = iteration.time() * iteration.time_unit_SI()
            else:
                time = iteration.time * iteration.time_unit_SI
            timevalues.append(time)
            self._timemap[time] = idx     

        # Fields meta-data
            for mesh_name, mesh in iteration.meshes.items():
                if (need_update):
                     fields.append(mesh_name)
        # Species meta-data                     
            for species_name, _ in iteration.particles.items():
                if (need_update):
                    species.append(species_name)
        # Particle records meta-data
            for species_name, species in iteration.particles.items():
                 for record_name, record in species.items():
                     if (need_update):
                         particles.append(species_name + "_" + record_name)
            need_update=False
            
        self._timevalues = timevalues
        self._iterations = iterations
        self._species = species
        self._particles = particles
        self._fields = fields

    def get_series(self):
        return self._series

    def get_iterations(self):
        return self._iterations

    def get_filename(self):
        return self._filename    
    
    def get_timestep_values(self):
        return self._timevalues        

    def get_species(self):
        return self._species        

    def get_particles_records(self):
        return self._particles

    def get_fields(self):
        return self._fields    
    
    def get_n_part(self, itr, species):
        sp = itr.particles[species]
        var = sp["position"]
        return var["x"].shape[0]

    def particles_to_df(self, itr, species):
        sp = itr.particles[species]
        df = sp.to_df()
        return df
    
    def get_part_list(self, itr, species, name, start, end):
        sp = itr.particles[species]
        var = sp[name]
        arrays = []
        for name, scalar in var.items():
            comp = scalar.load_chunk([start], [end])
            self._series.flush()
            comp = comp * scalar.unit_SI
            arrays.append(comp)

        ncomp = len(var)
        if ncomp > 1:
            flt = np.ravel(arrays, order="F")
            return flt.reshape((flt.shape[0] // ncomp, ncomp))
        else:
            return arrays[0]

    def get_part_pos_corrected(self, itr, species, start, end):
        sp = itr.particles[species]
        var = sp["position"]
        ovar = sp["positionOffset"]
        position_arrays = []
        for name, scalar in var.items():
            pos = scalar.load_chunk([start], [end])
            self._series.flush()
            pos = pos * scalar.unit_SI
            off = ovar[name].load_chunk([start], [end])
            self._series.flush()
            off = off * ovar[name].unit_SI
            position_arrays.append(pos + off)

        flt = np.ravel(position_arrays, order="F")
        num_components = len(var)  # 1D, 2D and 3D positions
        flt = flt.reshape((flt.shape[0] // num_components, num_components))
        # 1D and 2D particles: pad additional components with zero
        while flt.shape[1] < 3:
            flt = np.column_stack([flt, np.zeros_like(flt[:, 0])])
        return flt


    def get_size(self, x_loc):
        
        #
        # Gather size information
        #
        data_list = []
         
        # Initialize the np arrays that will store the results:
        x_size      = np.array(0.0,'i')
        x_loc_size  = int(x_loc.size)
        # perform the reductions:
        comm.Allreduce([x_loc_size, MPI.INT], [x_size, MPI.INT], op=MPI.SUM)
        data_list.append(x_size)
        return(data_list)


    def gather_array(self, itr, x_loc ):
        #
        # Gather global array info from array/proc
        # Local arrays could be of differents size
        #
        data_list = []

        # Enforce numpy array
        sendbuf = np.array(x_loc)

        # Rank 0 only will  receive
        root = 0
        
        # Collect local array sizes using collective gather op.
        arr_sizes = np.array(comm.gather(len(sendbuf), root))

        '''
        if root == comm.rank:
            print("local array sizes:", arr_sizes)
        '''
        
        if rank == root:
            #print("array_sizes: {}, total: {}".format(arr_sizes, sum(arr_sizes)))
            recvbuf = np.empty(sum(arr_sizes), 'd')
        else:
            recvbuf = None

        comm.Gatherv(sendbuf=sendbuf, recvbuf=(recvbuf, arr_sizes), root=root)

        '''
        if rank == root:
            print("Gathered array: {}".format(recvbuf))
        '''         

        if root == comm.rank:
            recvbuf = recvbuf[np.where(recvbuf != 0)]
            recvbuf_size = recvbuf.size
        else:
            recvbuf_size = None
           
        data_list.append(recvbuf)
        data_list.append(recvbuf_size)
        return(data_list)        


    def get_nonzeros(self,x_loc):
        #
        # Check if all array/rank are non-null
        #
        data_list = []
        
        # Initialize the np arrays that will store the results:
        n_nonzeros      = np.array(0,'i')
        
        # Get local non zeros counts
        n_nonzeros_loc = np.array([np.count_nonzero(x_loc)],'i')  
        #n_nonzeros_loc = np.max(n_nonzeros_loc, initial=-1*sys.maxsize)

        #print("rank: {} non_zeros {} ".format(rank, n_nonzeros_loc))
        # perform the reductions:
        comm.Allreduce(n_nonzeros_loc, n_nonzeros, op=MPI.SUM)
        #print("rank: {} n_nonzeros".format(rank, n_nonzeros))        

        data_list.append(n_nonzeros)
        return(data_list)
    
    def get_limits(self,x_loc):
        #
        # Gather array boundaries information
        #
        data_list = []
        
        # Initialize the np arrays that will store the results:
        x_min      = np.array(0.0,'d')
        x_max      = np.array(0.0,'d')

        # CHECK ME !!
        x_loc_max = np.max(x_loc,initial=-1*sys.float_info.max)
        x_loc_min = np.min(x_loc,initial=+1*sys.float_info.max)

        print("rank: {} local_max: {} local_min: {}".format(rank, x_loc_max, x_loc_min))
        # perform the reductions:
        comm.Allreduce(x_loc_min, x_min, op=MPI.MIN)
        comm.Allreduce(x_loc_max, x_max, op=MPI.MAX)
        print("rank: {} x_max: {} x_min: {}".format(rank, x_max, x_min))        

        data_list.append(x_min)
        data_list.append(x_max)
        return(data_list)

    
    def get_xyz_limits(self,x_loc, y_loc, z_loc):
        #
        # Gather XYZ boundaries information
        #
        data_list = []
        
        # Initialize the np arrays that will store the results:
        x_min      = np.array(0.0,'d')
        x_max      = np.array(0.0,'d')
        y_min      = np.array(0.0,'d')
        y_max      = np.array(0.0,'d')
        z_min      = np.array(0.0,'d')
        z_max      = np.array(0.0,'d')        

        x_loc_max = np.max(x_loc)
        x_loc_min = np.min(x_loc)
        y_loc_max = np.max(y_loc)
        y_loc_min = np.min(y_loc)
        z_loc_max = np.max(z_loc)
        z_loc_min = np.min(z_loc)        
        
        # perform the reductions:
        comm.Allreduce(x_loc_min, x_min, op=MPI.MIN)
        comm.Allreduce(x_loc_max, x_max, op=MPI.MAX)
        comm.Allreduce(y_loc_min, y_min, op=MPI.MIN)
        comm.Allreduce(y_loc_max, y_max, op=MPI.MAX)
        comm.Allreduce(z_loc_min, z_min, op=MPI.MIN)
        comm.Allreduce(z_loc_max, z_max, op=MPI.MAX)

        data_list.append(x_min)
        data_list.append(x_max)
        data_list.append(y_min)
        data_list.append(y_max)
        data_list.append(z_min)
        data_list.append(z_max)

        return(data_list)

    def get_particles(self, itr):
        data_list = []
        # Read particles information in pieces  
        n_part  = 0
        d_npart = 0

        n_part = self.get_n_part(itr,"electrons")    
        d_npart = n_part // comm.size
    
        start = comm.rank * d_npart
        if comm.rank < (comm.size - 1 ):
            end = d_npart
        else:
            end = n_part - (comm.size-1) * d_npart
            
        # Get kinematics info    
        part_pos = self.get_part_list(itr,"electrons","position", start, end)         
        part_p   = self.get_part_list(itr,"electrons","momentum", start, end)
        part_w   = self.get_part_list(itr,"electrons","weighting", start, end)

        # Get Max Min from x and y
        x_loc  = part_pos[...,0]
        y_loc  = part_pos[...,1]
        z_loc  = part_pos[...,2]        
        px_loc = part_p[...,0]
        py_loc = part_p[...,1]
        pz_loc = part_p[...,2]        
        w_loc  = part_w 

        # Move to contiguous
        x_loc = np.transpose(x_loc)
        y_loc = np.transpose(y_loc)
        z_loc = np.transpose(z_loc)        
        px_loc = np.transpose(px_loc)
        py_loc = np.transpose(py_loc)
        pz_loc = np.transpose(pz_loc)        
        #w_loc = np.transpose(w_loc) # don't need to transpose weights
          
        x_loc = np.ascontiguousarray(x_loc)
        y_loc = np.ascontiguousarray(y_loc)
        z_loc = np.ascontiguousarray(z_loc)        
        px_loc = np.ascontiguousarray(px_loc)
        py_loc = np.ascontiguousarray(py_loc)
        pz_loc = np.ascontiguousarray(pz_loc)        
        w_loc = np.ascontiguousarray(w_loc)

        data_list.append(x_loc)
        data_list.append(y_loc)
        data_list.append(z_loc)
        data_list.append(px_loc)
        data_list.append(py_loc)
        data_list.append(pz_loc)
        data_list.append(w_loc)        
        
        return(data_list)
    

    def get_array(self, var, chunk_offset, chunk_extent):        
        arrays = []
        for name, scalar in var.items():
            comp = scalar.load_chunk(chunk_offset, chunk_extent)
            self._series.flush()
            comp = comp * scalar.unit_SI
            arrays.append(comp)

        ncomp = len(var)
        if ncomp > 1:
            flt = np.ravel(arrays, order="F")
            return flt.reshape((flt.shape[0] // ncomp, ncomp))
        else:
            return arrays[0].flatten(order="F")              


    def read_3d_field(self, itr, field_name, component_name):
        field = itr.meshes[field_name]
        
        if field.scalar:
            component = next(field.items())[1]
        else:
            component = field[component_name]
            
        # Get the global shape of the dataset
        global_shape = component.shape  # (nz, ny, nx)

        # Determine the local dimensions for each rank
        nz, ny, nx = global_shape
        local_nz = nz // comm.size  # Assuming even distribution across ranks
        local_offset = comm.rank * local_nz  # Offset for this rank

        # Handle the case where nz is not perfectly divisible by size
        if comm.rank == size - 1:  # Last rank takes the remainder
            local_nz += nz % size
            
            # Define the extent and offset for the slice
            offset = (local_offset, 0, 0)  # (z_offset, y_offset, x_offset)
            extent = (local_nz, ny, nx)     # (z_extent, y_extent, x_extent)

            # Load the chunk of data for this rank
            #Ey_local = component.load_chunk(offset=offset, extent=extent)
            Ey_local = component.load_chunk()
            # Now you can work with Ey_local, which contains the local slice of the data
            print(f"Rank {rank} read Ey_local shape: {Ey_local.shape}")
            
        
    def get_field(self, itr, field_name, component_name):

        field = itr.meshes[field_name]
         
        if field.scalar:
            component = next(field.items())[1]
        else:
            component = field[component_name]
            
        # Dimensions of the grid
        shape = component.shape
        grid_spacing = field.grid_spacing
        global_offset = field.grid_global_offset
        grid_unit_SI = field.grid_unit_SI
        grid_position = component.position
        time = (itr.time + field.time_offset) * itr.time_unit_SI
        field_attrs = {a: field.get_attribute(a) for a in field.attributes}
        component_attrs = {a: component.get_attribute(a) for a in component.attributes} 

        # Print Meta data info values        
        if 0 == comm.rank:
            print("Field component shape", shape)
            print("grid spacing: ",grid_spacing)
            print("global_offset: ",global_offset)
            print("grid_unit: ",grid_unit_SI)
            print("grid_position: ",grid_position)
            print("time: ",time)

        # Chunk data for parallel reading
        dz = shape[0] // comm.size
        dz_leftover= shape[0]%comm.size
        dz_sizes0 = np.ones(comm.size, dtype='i')*dz
        dz_sizes  = np.ones(comm.size, dtype='i')*dz        
        dz_sizes[:dz_leftover]+=1

        chunk_offset = [comm.rank * dz_sizes[comm.rank], 0, 0]
        chunk_extent = [dz_sizes[comm.rank], shape[1], shape[2]]

        chunk_data = component.load_chunk(chunk_offset, chunk_extent)
        self._series.flush()

        
        # Linearize the data chunks
        # Slicing / mid-y slice
        sendbuf = np.ascontiguousarray(chunk_data[:,:,shape[2]//2])
        # Linearization 
        sendbuf = sendbuf.ravel(order='C')
        
        '''
        # Debug info 
        if 0 == comm.rank:
            print(" rank 0 dz_sizes0 : ", dz_sizes0)
            print(" rank 0 dz_sizes (+leftover) : ", dz_sizes)
            print(" rank {} sendbuf size: {} re-calc: {} ".format(comm.rank, sendbuf.size, dz_sizes[comm.rank]*shape[1]))
            print(" rank 0 sendbuf len: {}  val: {} ", len(sendbuf), sendbuf)
        '''
        
        # Offsets
        offsets = np.zeros(comm.size, dtype='i')
        offsets[1:] = np.cumsum(dz_sizes*shape[1])[:-1]
        total_2d_len    = shape[0]*shape[1]        
        total_buflen = sum(dz_sizes) * shape[1] # that's equivalent
        #print(" total_2d_len: {} total_buflen: {}".format(total_2d_len, total_buflen))

        # Allgatherv
        recvbuf = np.empty(total_buflen, dtype='d')                    
        #comm.Allgatherv(sendbuf=sendbuf, recvbuf=([recvbuf, dz_sizes*shape[1], offsets, MPI.DOUBLE]))
        comm.Gatherv(sendbuf=sendbuf, recvbuf=([recvbuf, dz_sizes*shape[1], offsets, MPI.DOUBLE]), root=0)                

        # Get the full field_data for comparison
        # !Check me! : should be still an option ...
        
        #data = self.get_chunk_slices(component)
        #data_r = data[:,:,shape[2]//2]
        #data_r = data_r.ravel(order='C')

        
        if 0 == comm.rank:
            #print(" recvbuf len: {} {} data_r size".format(recvbuf.size, data_r.size))
            recvbuf = recvbuf.reshape((shape[0],shape[1]))
        
        # Add axis label ( should be an argument )
        axis_labels = ['z', 'x', 'y']
        # Create the axes  Dict. out of it
        axes = { i: axis_labels[i] for i in range(len(axis_labels)) }
        info = FieldMetaInformation( axes, component.shape,
                                     grid_spacing, global_offset,
                                     grid_unit_SI, grid_position,
                                     time, itr, field_attrs=field_attrs,
                                     component_attrs=component_attrs )
        return recvbuf, info, chunk_data

    def get_chunk_slices(self, component):
        # Chunks in record.component
        #print("rank {} chunks".format(comm.rank))
        chunks = component.available_chunks()

        # Create Field data store
        NaN_value = np.nan if np.issubdtype(component.dtype, np.floating) or \
            np.issubdtype(component.dtype, np.complexfloating) else 0

        # Get the full contents of the field
        field_data = np.full(component.shape, NaN_value, component.dtype)        
        for chunk in chunks:
            chunk_slice = self.chunk_to_slice(chunk)

            #if 0 == comm.rank:                
                #print(chunk_slice)
                
            volume=1     
            for csl in chunk_slice:
                volume *= csl.stop - csl.start
                #print(" volume{} start{}  stop {}".format(volume, csl.start, csl.stop))
            # read only valid region
            x = component[chunk_slice]
            self._series.flush()
            field_data[chunk_slice] = x

        return field_data 
    
    def chunk_to_slice(self,chunk):
        """
        Convert an openPMD_api.ChunkInfo to slice
        """
        stops = [a + b for a, b in zip(chunk.offset, chunk.extent)]
        indices_per_dim = zip(chunk.offset, stops)
        index_tuple = map(lambda s: slice(s[0], s[1], None), indices_per_dim)
        return tuple(index_tuple)

    def write_field(self,  itr_o, field_name, component_name, data_local, field):
        if field.scalar:
            component = next(field.items())[1]
        else:
            component = field[component_name]
            
        # Dimensions of the grid
        shape = component.shape
        grid_spacing = field.grid_spacing
        global_offset = field.grid_global_offset
        grid_unit_SI = field.grid_unit_SI
        grid_position = component.position
        #time = (itr.time + field.time_offset) * itr.time_unit_SI
        field_attrs = {a: field.get_attribute(a) for a in field.attributes}
        component_attrs = {a: component.get_attribute(a) for a in component.attributes} 

        # Print Meta data info values        
        if 0 == comm.rank:
            print("WRITING")
            print("Field component shape", shape)
            print("grid spacing: ",grid_spacing)
            print("global_offset: ",global_offset)
            print("grid_unit: ",grid_unit_SI)
            print("grid_position: ",grid_position)
            #print("time: ",time)

        # Create the output structure
        #field_o = itr_o.meshes[field_name]
        #component_o = field_o[component_name] 
                   
        # Print Meta data info values        
        if 0 == comm.rank:
            print("Field component shape", shape)
            
        # Chunk data for parallel writing
        dz = shape[0] // comm.size
        dz_leftover = shape[0] % comm.size
        dz_sizes = np.ones(comm.size, dtype='i') * dz
        dz_sizes[:dz_leftover] += 1
        
        # Offsets and extents for the current rank
        chunk_offset = [comm.rank * dz_sizes[comm.rank], 0, 0]  # Adjust for 3D
        chunk_extent = [dz_sizes[comm.rank], shape[1], shape[2]]

        # Get input data
        print("chunk_data", data_local)
        #self._series.flush()
                
        # Prepare the chunk of data for the current rank
        #component_o.storeChunk(chunk_data, chunk_offset, chunk_extent);

        # Return any necessary information or status
        return True
    
