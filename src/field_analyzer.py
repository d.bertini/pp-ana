# Field_analyzer.py

import os
import numpy as np
from pathlib import Path
import scipy.constants as const
import dask.dataframe as dd
from mpi4py import MPI
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from src.opmd_meta_data import opmd_meta_data, load_metadata  


class FieldAnalyzer:
    """
    A class to analyze field data from parquet files in a parallel computing environment using MPI.

    Attributes:
    ----------
    pq_dir : Path
        The directory containing the parquet files for field and metadata.
    output_dir : Path
        The directory where output plots will be saved.
    comm : MPI.Comm
        The MPI communicator for parallel processing.
    rank : int
        The rank of the current process in the MPI environment.
    size : int
        The total number of processes in the MPI environment.
    local_meta_df : dask.dataframe
        Dask DataFrame containing local metadata.
    local_meta_data : DataFrame
        Pandas DataFrame containing computed local metadata.
    local_field_data : DataFrame
        Pandas DataFrame containing computed local field data.

    Methods:
    -------
    read_metadata():
        Reads metadata from a parquet file and stores it in local attributes.
    
    read_fields():
        Reads field data from a parquet file and stores it in local attributes.
    
    extract_field_data(component):
        Extracts and linearizes field data for a specified component, 
        gathers data across all MPI processes, and returns the gathered data.
    
    plot_field_data(field_type, component, field_data, field_info):
        Plots the field data and saves the plot to the output directory.
    """

    def __init__(self, pq_dir, output_dir):
        """
        Initializes the FieldAnalyzer with the specified directories.

        Parameters:
        ----------
        pq_dir : str
            The directory containing the parquet files for field and metadata.
        output_dir : str
            The directory where output plots will be saved.
        """
        self.pq_dir = Path(pq_dir)
        self.output_dir = Path(output_dir)
        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()
        self.size = self.comm.Get_size()
        self.local_meta_df = None
        self.local_meta_data = None
        self.local_field_data = None        

    def read_metadata(self):  
        """
        Reads metadata from a parquet file specific to the current MPI rank.
        
        The metadata is stored in the local_meta_df and local_meta_data attributes.
        If the metadata DataFrame is empty, a message is printed to the console.
        """
        meta_file = f"{self.pq_dir}/e_meta_info_{self.rank}.parquet"
        meta_df = dd.read_parquet(meta_file, engine='pyarrow')
        self.local_meta_df = meta_df
        self.local_meta_data = meta_df.compute()

        if self.local_meta_data.empty:
            print(f"Rank {self.rank}: The meta dataframe is empty.")
        else:
            print(f"Rank {self.rank}: The meta dataframe has {len(self.local_meta_data)} rows.")

    def read_fields(self):  
        """
        Reads field data from a parquet file specific to the current MPI rank.
        
        The field data is stored in the local_fields_data attribute.
        If the field data DataFrame is empty, a message is printed to the console.
        """

        fields_file = f"{self.pq_dir}/fields_rank_{self.rank}.parquet"
        fields_df = dd.read_parquet(fields_file, engine='pyarrow')
        self.local_fields_data = fields_df.compute()

        if self.local_fields_data.empty:
            print(f"Rank {self.rank}: The fields dataframe is empty.")
        else:
            print(f"Rank {self.rank}: The fields dataframe has {len(self.local_fields_data)} rows.")

    def extract_field_data(self, component):
        """
        Extracts and linearizes field data for a specified component.

        Parameters:
        ----------
        component : str
            The name of the field component to extract (e.g., 'Ex', 'Ey', 'Ez').

        Returns:
        -------
        tuple
            A tuple containing:
            - recvbuf : np.ndarray
                The gathered field data across all MPI processes.
            - metadata : object
                The metadata object containing field dimensions.
            - chunk_data : np.ndarray
                The local chunk of field data for the specified component.
        """
        metadata = load_metadata(self.local_meta_df)  
        print(metadata)

        local_shape  = np.array([metadata.ldim_x, metadata.ldim_y, metadata.ldim_z])
        global_shape = np.array([metadata.gdim_x, metadata.gdim_y, metadata.gdim_z])

        chunk_data = self.local_fields_data[component].values.reshape(local_shape)

        sendbuf = np.ascontiguousarray(chunk_data[:,:,local_shape[2]//2])
        sendbuf = sendbuf.ravel(order='C')

        dz = global_shape[0] // self.comm.size
        dz_leftover = global_shape[0] % self.comm.size
        dz_sizes0 = np.ones(self.comm.size, dtype='i') * dz
        dz_sizes = np.ones(self.comm.size, dtype='i') * dz        
        dz_sizes[:dz_leftover] += 1
        
        # Offsets for gathering data
        offsets = np.zeros(self.comm.size, dtype='i')
        offsets[1:] = np.cumsum(dz_sizes * global_shape[1])[:-1]
        total_buflen = sum(dz_sizes) * global_shape[1]  # Total buffer length for gathered data
        
        # Allgatherv to gather data from all processes
        recvbuf = np.empty(total_buflen, dtype='d')                    
        self.comm.Gatherv(sendbuf=sendbuf, recvbuf=([recvbuf, dz_sizes * global_shape[1], offsets, MPI.DOUBLE]), root=0)                        
        
        if 0 == self.comm.rank:
            recvbuf = recvbuf.reshape((global_shape[0], global_shape[1]))

        return recvbuf, metadata, chunk_data

    def plot_field_data(self, field_type, component, field_data, field_info):
        """
        Plots the field data and saves the plot to the output directory.

        Parameters:
        ----------
        field_type : str
            The type of field being plotted (e.g., 'E' for electric field).
        component : str
            The component of the field being plotted (e.g., 'x', 'y', 'z').
        field_data : np.ndarray
            The field data to be plotted.
        field_info : object
            An object containing metadata about the field, including min and max values for axes and time.
        """
        if self.rank == 0:
            # Create output directory based on field type and component
            save_dir = self.output_dir / f'{field_type}-field/{field_type}{component}/'
            if not os.path.isdir(save_dir):
                os.makedirs(save_dir)
            
            # Normalization constants (example for electric fields)
            if field_type == 'E':
                max_e_dens = 1e25
                wp = np.sqrt(max_e_dens * const.e**2 / const.epsilon_0 / const.m_e)
                field_normalization = const.e / const.m_e / const.c / wp
            else:
                # Add normalization for magnetic fields if needed
                field_normalization = 1  # Magnetic field normalization

            plt.imshow(field_data.T * field_normalization, 
                       extent=[field_info.z_min * 1e6, 
                               field_info.z_max * 1e6, 
                               field_info.x_min * 1e6, 
                               field_info.x_max * 1e6],                        
                       aspect='equal')                       
            fig = plt.gcf()
            fig.set_size_inches(19, 4)            
            plt.colorbar(label=f'{field_type} field in V/m' if field_type == 'E' else f'{field_type} field in T')
            plt.title(f'{field_type} field, time = {np.round(field_info.time * 1e12, 2)} ps')
            plt.xlabel('z in µm')
            plt.ylabel('x in µm')
            plt.savefig(save_dir / f'{field_type}{component}.png')
            plt.close()

