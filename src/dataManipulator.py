import numpy as np
import math
import matplotlib.pylab as pltc
import scipy as sp
import scipy.constants as const
# import scipy.interpolate as inter
#import scipy.optimize as spo
import scipy.ndimage as spi
import copy
import time 
import sys
#from scipy.optimize import curve_fit
import matplotlib
matplotlib.use('Agg')
c = 299792458 # m/s

# normalizes the data to its maximum
def norma(data):
	return data/np.max(data)

# returns the resolution of data: daxis[0], daxis[1] if 2 dimensional
def resolution(data):
	if not np.shape(data[0]):
		return abs(data[1]-data[0]) 
	else:
		return abs(data[0][1]-data[0][0]), abs(data[1][1]-data[1][0])

# return the size of the window of data (x and y)
def window(data):
	length = len(np.shape(data))
	if len(np.shape(data)) ==2:
		return abs(data[0][-1]-data[0][0]), abs(data[1][-1]-data[1][0])
	elif len(np.shape(data)) ==1:
		return abs(data[-1]-data[0]) 
	else:
		print('Error in resolution: 3D not possible!')

def calc_intensity(electric_field):
	return c*0.5*const.epsilon_0*electric_field**2

# Summarize the data over one Axis defined by 'Y' or 'X'
def sum_over_axis(data, axis):
	if axis == 'x' or axis == 'X':
		return np.sum(data,axis=1)
	elif axis == 'y' or axis == 'Y':
		return np.sum(data,axis=0)
	else:
		print('Error in sum_over_axis: Axis not correctly specified!')

# calculate the axis after a fourier transformation
# multiplier: multiplier can be set to 1 if only frequency is used instead of angular frequency
def fft_axis(axis, multiplier = 2*math.pi):
	assert len(np.shape(axis)) <2, 'Error in fft_axis: Only defined for 1D arrays'
	N = int(len(axis))
	d = multiplier/window(axis)
	new_axis = (np.arange(N)-N/2)*d
	return new_axis

# do a fourier transformation in 1D or 2D
# multiplier: multiplier can be set to 1 if only frequency is used instead of angular frequency
def fftc(data,axis=None, multiplier = 2*math.pi):
	N = len(np.shape(data))
	# 1D or 2D fft
	if N <2:
		transformed_data= np.fft.fftshift(np.fft.fft(np.fft.ifftshift(data)))
	else:
		transformed_data = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(data)))

	# if an axis is specified, also return the fourier-transformed axis
	if np.all(axis != None):
		new_axis = [ fft_axis(x, multiplier = multiplier) for x in axis] if N > 1 else fft_axis(axis, multiplier = multiplier)
		return transformed_data, new_axis
	else:
		return transformed_data

# do an inverse fourier transformation in 1D or 2D
def ifftc(data, axis=None, multiplier = 2*math.pi):
	N = len(np.shape(data))
	mod2 = np.mod(len(data),2) == 1	# needs an ifftshift if the array is odd
	if N <2:
		if mod2:
			transformed_data = np.fft.fftshift(np.fft.ifft(np.fft.ifftshift(data)))
		else:
			transformed_data = np.fft.ifftshift(np.fft.ifft(np.fft.fftshift(data)))
	else:
		if mod2:
			transformed_data= np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(data)))
		else:
			transformed_data= np.fft.ifftshift(np.fft.ifft2(np.fft.fftshift(data)))

	# if an axis is specified, also return the fourier-transformed axis
	if np.all(axis != None):
		new_axis = [ fft_axis(x, multiplier = multiplier) for x in axis] if N > 1 else fft_axis(axis, multiplier = multiplier)
		return transformed_data, new_axis
	else:
		return transformed_data

# find the closest value which is dividable by 2
def next_power_of_2(x):  
    return 1 if x == 0 else 2**(int(x) - 1).bit_length()

# pad axis with a linear ramp
# axis: the axis you want to pad
# padleft: pads the axis on the left side by XX elements (integer = True) or by XX units (integer = False)
# padright: " right "
# integer: boolean if the padsize is given as integer or not
# nearest_pow2: boolean if the padwidth should be dividible by 2
def pad_axis(axis, padleft = 0, padright = 0, integer = False, nearest_pow2 = False):
	d = resolution(axis)
	if  not integer:
		padleft = int(padleft/d)
		padright = int(padright/d)

	if nearest_pow2:
		next_pow2 = next_power_of_2(len(axis)+padleft+padright)-len(axis)
		padleft = np.floor(float(next_pow2)/2)
		padright = np.ceil(float(next_pow2)/2)

	return np.pad(axis,(padleft, padright), 'linear_ramp', end_values=(-padleft*d+axis[0], padright*d+axis[-1]))

# pads the data and axis with 0s
# matrix: the data you want to pad
# axis: the corresponding axis to the data
# padwidthXleft: pads the X axis on the left side by XX elements (integer = True) or by XX units (integer = False)
# padwidthXright: " right "
# padwidthYdown: pads the Y axis on the lower side by XX elements (integer = True) or by XX units (integer = False)
# padwidthYup: " upper "
# integer: boolean if the padsize is given as integer or not
# nearest_pow2: boolean if the padwidth should be dividible by 2
def pad_data_and_axis(matrix,axis, padwidthXleft = 0, padwidthXright = 0, padwidthYdown = 0, padwidthYup = 0, integer = False, nearest_pow2 = False):
	d = resolution(axis)
	# 1D padding or 2D padding
	if not(np.shape(d)):
		if  not integer:
			padwidthXleft = int(padwidthXleft/d)
			padwidthXright = int(padwidthXright/d)

		if nearest_pow2:
			next_pow2 = next_power_of_2(len(axis)+padwidthXleft+padwidthXright)-len(axis)
			padwidthXleft = int(np.floor(float(next_pow2)/2))
			padwidthXright = int(np.ceil(float(next_pow2)/2))
		
		newAxis = pad_axis(axis, padleft = padwidthXleft, padright = padwidthXright, integer = True)
		
		return np.pad(matrix, (padwidthXleft,padwidthXright), 'constant'), newAxis
	else:
		if  not integer:
			padwidthYdown = int(padwidthYdown/d[1])
			padwidthYup = int(padwidthYup/d[1])
			padwidthXleft = int(padwidthXleft/d[0])
			padwidthXright = int(padwidthXright/d[0])
			
		if nearest_pow2:
			next_pow2_x = next_power_of_2(len(axis[0])+padwidthXleft+padwidthXright)-len(axis[0])

			padwidthXleft = int(np.floor(float(next_pow2_x)/2))
			padwidthXright = int(np.ceil(float(next_pow2_x)/2))

			next_pow2_y = next_power_of_2(len(axis[1])+padwidthYdown+padwidthYup)-len(axis[1])

			padwidthYdown = int(np.floor(float(next_pow2_y)/2))
			padwidthYup = int(np.ceil(float(next_pow2_y)/2))
		
		newXAxis = pad_axis(axis[0],padleft = padwidthXleft, padright = padwidthXright, integer = True)
		newYAxis = pad_axis(axis[1],padleft = padwidthYdown, padright = padwidthYup, integer = True)

		return np.pad(matrix, ((padwidthYdown,padwidthYup),(padwidthXleft,padwidthXright)), 'constant'), [newXAxis, newYAxis]	

# cuts out a part of a matrix and corresponding axis		
# matrix: data to cut out from
# axis: the corresponding axis
# xmin: minimum cutout value in X (in units)
# xmax: maximum cutout value in X (in units)
# ymin: minimum cutout value in Y (in units)
# ymax: maximum cutout value in Y (in units)
def cut_out(matrix, axis, xmin= None, xmax= None, ymin= None, ymax = None, compression =[None, None]):
	# get the corresponding indices
	xMinInd = np.argmin(abs(axis[0]-xmin)) if xmin else 0
	xMaxInd = np.argmin(abs(axis[0]-xmax)) if xmax else len(axis[0])
	yMinInd = np.argmin(abs(axis[1]-ymin)) if ymin else 0
	yMaxInd = np.argmin(abs(axis[1]-ymax)) if ymax else len(axis[1])
	return  matrix[yMinInd:yMaxInd,xMinInd:xMaxInd], [axis[0][xMinInd:xMaxInd], axis[1][yMinInd:yMaxInd]]
	
# Compresses the data and the axis by the given compression factor if its modulo is 0
def compress(data,axis, compression):
	shape = np.shape(data)
	compShape = np.shape(compression)
	if len(shape) == 1:
		#  check if array is dividable by the compression factor
		if shape[0] % compression != 0:
			print("Compression Failed! Data not divisible by compression-factor")
			sys.exit()
		else:
			return np.mean(data.reshape(-1,compression),axis=1).T
	else:

		# check if the compression different for both axes
		if len(compShape) >0:
			if compShape[0] ==2:
				compressionX = compression[0]
				compressionY = compression[1]
				if len(data[0]) % compressionX != 0 and len(data[1]) % compressionY != 0:
					print("Compression Failed! Data not divisible by compression-factor")
					sys.exit()
		else:
			compressionX = compression
			compressionY = compression
			if len(data[0]) % compression != 0 and len(data[1]) % compression != 0:
				print("Compression Failed! Data not divisible by compression-factor")
				sys.exit()
			
		# first compress in one direction of the matrix
		data = np.mean(data.reshape((shape[0],-1,compressionX)),-1)
		
		data = np.mean((data.T).reshape(((data.T).shape[0],-1,compressionY)),-1)

		axis[0] = np.mean(axis[0].reshape(-1,compressionX),axis=1).T

		axis[1] = np.mean(axis[1].reshape(-1,compressionY),axis=1).T

		return data.T, axis

# removes the oscillation from a field by setting everything to 0 in the spectral domain which does not fulfill the following values
#  wmin < sqrt(wX^2+wY^2) < wmax  and wX > 0
def remove_oscillation(field,axis = None, wmin = None, wmax = None, propagationAxis=0, w0 = 2*math.pi*c/1053e-9):
	# since we remove all negative frequency, we also lose half of the energy in the field
	multiplier = 2
	
	# find the indices for wmin and wmax
	if wmin == None: wmin = w0-w0/4
	if wmax == None: wmax = w0+w0/2	
	
	# 2D removal
	if len(np.shape(field)) > 1:
		axisLen1 = np.shape(field)[0] if propagationAxis else np.shape(field)[1]
		
		# transform to the spectral domain, if an axis is set, the frequencies outside of wmax and wmin are also set to 0
		if np.all(axis != None):
			field, [wX,wY] = fftc(field, axis = axis)
			field = set_spec_to_zero(field,[wX,wY], wmin, wmax)
		else:
			field = fftc(field)
		# depending on the propagation axis, one part of the frequency is set to 0
		if propagationAxis == 1:	
			field[int(axisLen1/2):-1,:] = 0
		else:
			field[:,0:int(axisLen1/2)] = 0
	# 1D removal
	else:
		axisLen1 = len(field)
		if np.all(axis != None):
			field, wX = fftc(field, axis = axis)
			wmin_ind = np.argmin(abs(wX-wmin))
			wmax_ind = np.argmin(abs(wX-wmax))
			field[:wmin_ind-1] = 0
			field[wmax_ind+1:] = 0
		else:
			field =fftc(field)

		field[0:int(axisLen1/2)] = 0
	
	return ifftc(field)*multiplier # transform to the time domain and restore the initial energy

	
# TODO gscheit machen
# currently only 1D and shitty
# Removes the linear spectral Phase of a Field by moving its maximum of the time domain to the center of the window
def remove_linear_phase(field, axis, realpart=True, direction = 0):

	if not(realpart):
		maximum = np.argmax(abs(remove_oscillation(field))**2)
	else:
		maximum = np.argmax(abs(field))
		
	if np.shape(axis) == 1:
		length = len(axis)
		dx = abs(axis[1]-axis[0])
		
		if maximum <= length/2:
			padwidthX2 = 0
			padwidthX1 = length-2*maximum
		else:
			padwidthX1=0
			padwidthX2 = 2*maximum - length-1
		
		paddedField = np.pad(field, (padwidthX1,padwidthX2), 'constant')
		
		paddedAxis = np.pad(axis,(padwidthX1, padwidthX2), 'linear_ramp', end_values=(-padwidthX1*dx+axis[0], padwidthX2*dx+axis[-1]))
		
		return paddedField, paddedAxis
	else:
		lenY,lenX = np.shape(field)
		dx = resolution(axis[0])
		dy = resolution(axis[1])
		maximum = maximum%lenX
		if maximum <= lenX/2:
			padwidthX2 = 0
			padwidthX1 = lenX-2*maximum
		else:
			padwidthX1 = 0
			padwidthX2 = 2*maximum-lenX-1
		
		return padMatrixAndAxis(field, axis, padwidthXleft =  padwidthX1, padwidthXright = padwidthX2, integer = True)
		

# currently only 1D
# converts the Field from frequency / angular frequency to wavelength
def omega_to_lambda(spectrumOmega, omega, multiplier = 2*math.pi*c):
	l = multiplier/omega
	return (spectrumOmega*multiplier/l**2)[::-1], l[::-1]

# currently only 1D
# converts the Field from  wavelength to frequency / angular frequency
def lambda_to_omega(spectrumLambda, l, multiplier = 2*math.pi*c):
	return  (spectrumLambda*l**2/multiplier)[::-1], (multiplier/l)[::-1]

# returns the width in pixel of a 1D array by first searching on the right and then on the left of the maximum. If an axis is defined it also returns the indices of the left 
# and right half and also the FWHM in axis values interpolated for a better precision
def width(array, axis=[], factor = 0.5, returnIndices = False):
	maxInd = np.argmax(abs(array))

	if maxInd != 0:
		indLow = np.argmin(abs(array[:maxInd]-array[maxInd]*factor))
		indUp = np.argmin(abs(array[maxInd:]-array[maxInd]*factor))
		
		indUp = indUp+maxInd
		
		dInd = indUp-indLow
	else:
		indLow = 0
		indUp = 0
		dInd = 0

	if np.size(axis) != 0:		
		# interpolation between values (searches for the nearest pixel)
		if indLow == 0:
			xLow = axis[1]
		else:
			if abs(array[maxInd]*factor-array[indLow-1]) > abs(array[maxInd]*factor-array[indLow+1]):
				indLow = indLow-1

			m =( array[indLow+1]-array[indLow])/(axis[indLow+1]-axis[indLow])
			b = array[indLow]-m*axis[indLow]
			
			xLow = (array[maxInd]*factor-b)/m
		
		if indUp == len(array)-1:
			xUp = axis[indUp]
		else:
			if abs(array[maxInd]*factor-array[indUp-1]) < abs(array[maxInd]*factor-array[indUp+1]):
				indUp = indUp-1

			m =( array[indUp+1]-array[indUp])/(axis[indUp+1]-axis[indUp])
			b = array[indUp]-m*axis[indUp]
			
			xUp = (array[maxInd]*factor-b)/m
		
		if returnIndices:
			return xUp-xLow,indLow, indUp
		else:
			return xUp-xLow
	else:
		if returnIndices:
			return dInd, indLow, indUp
		else:
			return dInd

# calculates the 1d moment of the given order
def moment1d(data,axis,order):
	if order == 1:
		return (np.dot(abs(axis)**order,data))/np.sum(data)
	else:
		return (np.dot(abs(axis-moment1d(data,axis,1))**order,data))/np.sum(data)

# calculates the moment of the given order for 1D and 2D data
def moment(data,axis,order):
	if len(np.shape(data)) > 1:
		return moment1d(moment1d(np.sum(data,1),axis[1],order),moment1d(np.sum(data,0),axis[0],order),order)
	else:
		return moment1d(data,axis,order)

# calculate the width of a signal for a given method
def pulse_width(pulse, axis, method = 'rms'):
	if method.lower() == 'fwhm':
		return width(pulse, axis = axis, factor = 0.5)
	elif method.lower() == 'rms':
		return np.sqrt(moment(pulse,axis,2))
	elif method.lower() == 'effective':
		return np.sum(abs(pulse)*resolution(axis))/np.max(pulse)
	else:
		assert 'Method for calculating the pulse width is not defined!'


		

# requires spectrum on angular frequency and returns lambda
# returns the properties in the order (if specified) [central Wavelength in nm, bandwidth (given by the first moment) in nm, bandwidth given by an intensity threshold]
def spectrum_properties(spectrum, axis, central_wavelength_moment = True, bandwidth_sigma = True, bandwidth_threshold = 0.05, bandwidth_effective = True):
	output = []
 # # calculate the central wavelength of for the different spectra
	center = moment(spectrum,axis,1)

	if central_wavelength_moment:
		# # convert it to wavelength
		centerL = 2*math.pi*c/center
		output.append(centerL)

	if bandwidth_sigma:
		# # calculate the second moment of the spectrum
		dw = pulse_width(spectrum,axis, method = 'rms')

		output.append(dw*centerL**2/c/2*math.pi)

	if bandwidth_threshold != None:
		# calculate the width of the spectrum by using the X% of the intensity.
		bandwidth = width(spectrum, axis = axis, factor = bandwidth_threshold)

		output.append(bandwidth*centerL**2/2/math.pi/c)
	if bandwidth_effective:
		output.append(pulse_width(spectrum,axis, method = 'effective')*centerL**2/2/math.pi/c)
	return output

	
# TODO currently not used needs some improvment
# Cut out slices of the pulse and determine each spectrum / its center wavelength
def time_resolved_spectrum(field, axis, dt, center=False,w0 = 2*math.pi*c/1053e-9, wmin = None, wmax = None):

	N = int(resolution(axis[0])/c/dt)

	timeArray = (np.arange(N)+1)*dt
	if center == True:
		centerArray = np.zeros(N)
	spectrumList = [0]*N
	for ii in np.arange(N):
		cutField = np.zeros(np.shape(field))
		#print ii
		#print field
		cutField[:,N*ii:N*(ii+1)] = field[:,N*ii:N*(ii+1)]
		#cutField = field[:,N*ii:N*(ii+1)]

		transformed_field, [wX,wY] = fftc(cutField,axis/c)
		spectrum = abs(transformed_field)**2
		if center == True:
			if wmin != None and wmax != None:
				wminInd = np.argmin(abs(wmin-wX))
				wmaxInd = np.argmin(abs(wmax-wX))
				#print wminInd, wmaxInd
				centerArray[ii] =  2*math.pi*c/firstMoment(sum_over_axis(spectrum,'Y'), wX[wminInd:wmaxInd])*1e9
				spectrumList[ii] = sum_over_axis(spectrum[:,wminInd:wmaxInd],'Y')
			else:
				centerArray[ii] = 2*math.pi*c/firstMoment(sum_over_axis(spectrum,'Y'), wX)*1e9
				spectrumList[ii] = sum_over_axis(spectrum,'Y')

	return centerArray, timeArray, spectrumList

# TODO: clean up, cross-check and comment
# Get the location on the x-axis of a certain density within a certain y-area
#
# density: the input density as a matrix
# Axis: the input Axis for the matrix in a list of the form [1] = Y, and [0] = X
# n: the density or a list of densities you want to search the location of
# dn: the maximum deviation of the searched density, currently not a list
# y0: the y position you want to check your density
# dy: the area you want to check the density -> from y0-dy to y0+dy
# mean: if mean is set to False it will return the position of the FIRST searched density n for every found y value -> array(y), array(x(n[0]))
# method: the method to find the position with. Detailed explanations are given at the corresponding position
#		  most reliable method: 4, which also calculates the scale length at the position
def get_density_location(density, axis, n, dn, y0, dy,  method = 4, mean = True):
	y0ind = np.argmin(abs(axis[1])-y0)

	if not mean:
		dy = len(axis[1])/2
		if type(n) is np.ndarray:
			n = n[0]

	if dy == 0:
		cutDensity = density[y0ind-1:y0ind+1,:]
	else:
		dyinds = int(dy/abs(axis[1][1]-axis[1][0]))
	
		cutDensity = density[y0ind-dyinds:y0ind+dyinds,:]
	if type(n) is np.ndarray:
		positions = np.zeros(np.size(n))
		scaleLength = np.zeros(np.size(n))
		it = 0
		for ii in n:
			if type(dn) is np.ndarray:
				dnii = dn[it]
			else:
				dnii = dn

				
			## Option 1): find all positions where the density is inside a certain density range (n +- dn) and return the mean position
			if method == 1:
				yInds, xInds1 = np.where(cutDensity>=ii-dnii)
				yInds, xInds2 = np.where(cutDensity<=ii+dnii)
				xInds = np.intersect1d(xInds1,xInds2)
				positions[it] = np.sum(axis[0][xInds])/len(xInds)
			## Option 1) end
			
			## Option 2): Take the first position where no density in y-direction is smaller than the density you are looking for
			if method == 2:

				yInds, xInds = np.where(cutDensity>=ii)

				if len(xInds) == 0:
					positions[it] = np.nan
				else:
					ind = np.min(xInds)
					found = False
					while not(found) and ind < len(cutDensity[:,1]):
						slice = cutDensity[:,ind]-ii
						if any(x<-dnii for x in slice) == False:
							found = True
						else:
							ind +=1
					positions[it] = axis[0][ind]
			## Option 2) end
			
			## Option 3): Take the first position where the mean value of the density in y-direction is greater or equal to the density you are looking for
			if method == 3:
				yInds, xInds = np.where(cutDensity>=ii)
				if len(xInds) == 0:
					positions[it] = np.nan
				else:
					ind = np.min(xInds)
					found = False
					while not(found) and ind < len(cutDensity[:,1]):
						slice = cutDensity[:,ind]-ii
						if np.mean(slice) >= 0:
							found = True
						else:
							ind +=1
					positions[it] = axis[0][ind]
			# option 3) end
			
			## Option 4): Fit a exponential function to the densities and find nc + the scale length
			if method == 4:
				try:
					profile = np.sum(cutDensity[:],0)/np.shape(cutDensity)[0]
					logProfile = np.log10(profile)
					min_dens = 5e26
					max_dens = 2*np.max(n)

					diff1 = np.gradient(logProfile)
					ind_high1 = np.where(diff1 >= 0)[0]
					ind_high2 = np.where(axis[0] <= 2e-6)[0]
					ind_high3 = np.where(profile <= max_dens)[0]
					ind_high = np.intersect1d(ind_high1,ind_high2)
					ind_high = np.intersect1d(ind_high3, ind_high)[-1]

					ind_low = np.where(profile[:ind_high] >= min_dens)[0][0]
					found = False
					itti = 0
					while not found:
						if ~np.isnan(logProfile[ind_low+itti]):
							found = True
							ind_low = ind_low+itti
							# addon
							if np.any(np.isnan(logProfile[ind_low:ind_high])):
								found = False
								itti += 1
						else:
							itti += 1

					popt = np.polyfit(axis[0][ind_low:ind_high], logProfile[ind_low:ind_high],12)
					profile_log_fit = np.polyval(popt, axis[0])
					profile_fit = 10**profile_log_fit
					ind_pos = np.argmin(abs(profile_fit[ind_low:ind_high] - ii))+ind_low
					positions[it] = axis[0][ind_pos]

					scaleLength[it] = profile_fit[ind_pos]/abs((profile_fit[ind_pos]-profile_fit[ind_pos-1]+profile_fit[ind_pos+1]-profile_fit[ind_pos])/2)*resolution(axis[0])
				except:
					positions[it] = np.nan
					scaleLength[it] = np.nan
			## Option 4) end
			it+=1
		if method ==4:
			return positions, scaleLength
		else:
			return positions
	else:
		if not mean:
			lenY = len(cutDensity[:,1])
			positions = np.zeros(lenY)
			for ii in range(lenY):
				inds = np.where(cutDensity[ii,:]>= n)[0]
				if len(inds) < 1:
					positions[ii] = np.nan
				else:

					positions[ii] = axis[0][inds[0]]
			return positions
	
# interpolate a matrix by increasing the resolution by a given factor
# def interpolate_matrix(matrix, factor):
# 	shape = np.shape(matrix)
# 	values = np.reshape(matrix, (shape[0]*shape[1]))
# 	pts = np.array([[i,j] for i in np.linspace(0,1,shape[0]) for j in np.linspace(0,1,shape[1])])
# 	grid_y, grid_x = np.mgrid[0:1:shape[0]*factor*1j, 0:1:shape[1]*factor*1j]
# 	return inter.griddata(pts, values, (grid_y, grid_x), method='linear')

	
# not used anymore
# def angular_spectrum_old(matrix, axis, thetaMax, dTheta, interpolationMultiplier = 0):
# 	# calculate the angle in radiant
# 	thetaMax = float(thetaMax)/180*math.pi
# 	dTheta = float(dTheta)/180*math.pi
# 	shapeY,shapeX = np.shape(matrix)
	
# 	dwX = 2*math.pi*c/abs(axis[0][-1]-axis[0][0])
# 	dwY = 2*math.pi*c/abs(axis[1][-1]-axis[1][0])

# 	# only keep the postivie frequencies
# 	subMatrix = matrix[:,shapeX/2:]
	
# 	complexConverter = 1j if np.iscomplexobj(subMatrix) else 1
	
# 	if interpolationMultiplier != 0:
# 		if np.iscomplexobj(subMatrix):
# 			subMatrix = interpolate_matrix(abs(subMatrix), interpolationMultiplier)*np.exp(1j*np.angle(interpolate_matrix(subMatrix,interpolationMultiplier)))
# 		else:
# 			subMatrix = interpolate_matrix(subMatrix, interpolationMultiplier)
# 		dwY = dwY/interpolationMultiplier
# 		dwX = dwX/interpolationMultiplier
# 	ratio = dwY/dwX
	
# 	shapeY, shapeX = np.shape(subMatrix)
	
# 	wY = (np.arange(shapeY)-shapeY/2)*dwY
# 	wX = np.arange(shapeX)*dwX
	
# 	#spectrum = np.zeros(shapeX)*complexConverter
	
# 	X = np.arange(shapeX)
# 	# calculate the slope for each X value depending on theta and dTheta
# 	angle = (np.arange(int(2*thetaMax/dTheta))*dTheta-thetaMax)
	
# 	# spectrum for different angles
# 	angleSpectrum = np.zeros((len(angle),len(X)))*complexConverter
	
# 	countMatrix = np.ones((shapeY,shapeX))
	
# 	iter = 0
# 	for ii in angle:

# 		Y =(shapeY/2+1 + np.around(X/ratio*np.sin(ii))).astype(int)
# 		Xprojection =np.around( X*np.cos(ii)).astype(int)

# 		for jj in range(len(X)):
# 			if Y[jj] >= shapeY or Y[jj] < 0:
# 				angleSpectrum[iter,jj] = 0
# 			else:
# 				angleSpectrum[iter,jj] = subMatrix[Y[jj],Xprojection[jj]]*dTheta
# 		iter+=1

# 	return angleSpectrum*wX, wX, wY

# calculates the angular spectrum after 2D fourier transformation
# works with quick maff
def angular_spectrum(matrix, axis, thetaMax, dTheta):
	# calculate the angle in radiant and the corresponding angle axis of the new matrix
	thetaMax = float(thetaMax)/180*math.pi
	dTheta = float(dTheta)/180*math.pi
	N = int((2.0*thetaMax)/dTheta)	
	theta = np.linspace(-N*dTheta/2+dTheta/2, N*dTheta/2+dTheta/2, N, endpoint = False)
	thetaMax = abs(theta[0])

	shapeY,shapeX = np.shape(matrix)

	# resolution of the input axis
	dwX = abs(axis[0][1]-axis[0][0])
	dwY = abs(axis[1][1]-axis[1][0])

	# only keep the positive frequencies
	subMatrix = matrix[:,int(shapeX/2):]
	shapeY, shapeX = np.shape(subMatrix)

	# set up corresponding frequency axis of input matrix
	wY = (np.arange(shapeY)-shapeY/2)*dwY
	wX = np.arange(shapeX)*dwX

	# maximum frequency value for the new axis
	wmax = np.sqrt(max(wY)**2+max(wX)**2)

	w = np.linspace(0,wmax,shapeX, endpoint = False)

	# create a meshgrid for the angle and frequency axis
	[w_mat, angle_mat] = np.meshgrid(w, theta)

	[w_mat_ind, angle_mat_ind] = np.meshgrid(np.linspace(0,shapeX-1,shapeX, endpoint = False), np.linspace(0, N-1, N, endpoint = False))

	# flatten matrices 
	w_mat_ind = w_mat_ind.flatten().astype(int)
	angle_mat_ind = angle_mat_ind.flatten().astype(int)

	# pre-allocate the result matrix 
	result = np.zeros(np.shape(w_mat))
	result = result.flatten()
	
	# calculate the x and y values for the frequency and angle
	x_mat = (w_mat*np.cos(angle_mat)).flatten()
	y_mat = (w_mat*np.sin(angle_mat)).flatten()

	# transform to indices in the original spectral matrix
	x1_ind_mat = np.floor(x_mat/dwX).astype(int)
	y1_ind_mat = np.floor(y_mat/dwY).astype(int)

	# ignore every entry which is outside of the original spectral matrix
	x_inside = np.where(x1_ind_mat+1 < shapeX, True, False)
	y_inside = np.where(abs(y1_ind_mat+1) < shapeY/2, True, False)

	# shift the y-axis to 0 value in the center
	y_shift = np.min(np.floor(y_mat[y_inside]/dwY).astype(int))
	y1_ind_mat -= y_shift

	# obtain a matrix containing the conditions above
	inside = np.logical_and(x_inside, y_inside)

	# only keep the values which are inside the original matrix
	x_mat = x_mat[inside]
	y_mat = y_mat[inside]

	x1_ind_mat = x1_ind_mat[inside]
	y1_ind_mat = y1_ind_mat[inside]

	# prepare for bilinear interpolation of the data points
	# calculate the interpolation points in X and Y
	x1_mat = x1_ind_mat*dwX
	x2_mat = x1_mat + dwX
	y1_mat = (y1_ind_mat+y_shift)*dwY
	y2_mat = y1_mat + dwY

	f11_mat = subMatrix[y1_ind_mat, x1_ind_mat].flatten()
	f12_mat = subMatrix[y1_ind_mat+1, x1_ind_mat].flatten()
	f21_mat = subMatrix[y1_ind_mat, x1_ind_mat+1].flatten()
	f22_mat = subMatrix[y1_ind_mat+1, x1_ind_mat+1].flatten()

	result[inside] = f11_mat*(x2_mat-x_mat)*(y2_mat-y_mat)
	result[inside] += f21_mat*(x_mat-x1_mat)*(y2_mat-y_mat)
	result[inside] += f12_mat*(x2_mat-x_mat)*(y_mat-y1_mat)
	result[inside] += f22_mat*(x_mat-x1_mat)*(y_mat-y1_mat)
	result[inside] /= (x2_mat-x1_mat)*(y2_mat-y1_mat)

	# reshape the result to the original shape
	result = result.reshape(np.shape(w_mat))

	return result, w, theta*180/np.pi

# TODO No hard cutoffs! Maybe two 2D gaussian functions that reduce oscillations?
## Takes the fourier transformation of the temporal field and the corresponding frequency axes (2D) and sets everything outside of wmin and wmax to zero.
def set_spec_to_zero(spectrum, axis, wmin, wmax):
	binary_spec = np.sqrt(np.tile(axis[0]**2,(len(axis[1]),1))+np.tile(axis[1]**2,(len(axis[0]),1)).T) # 1 nicer yannik
	return np.where(binary_spec > wmax, 0, np.where(binary_spec < wmin, 0, spectrum))
			

# returns the doppler shift from the reflection of a moving critical density whose position (Velocity = False) or Velocity (Velocity = True) is given in Data.
# The shift is returned as delta omega or delta lambda depending on the variable Lambda
def doppler_shift(data, time, aslambda = True, velocity = False, w0 = 2*math.pi*c/1053e-9):
	if velocity:
		beta = data/c
	else:
		velocities = np.gradient(data, time[-2]-time[-1])
		beta = velocities/c
	factor = (1+beta**2-2*beta)/(1-beta**2)
	if aslambda:
		return 2*math.pi*c/(w0*factor)-2*math.pi*c/w0
	else:
		return w0*factor-w0

# Data binning by a given axis
def binning(data, axis, newAxis):
	dataBinned = np.zeros(len(newAxis))
	resolution = abs(newAxis[1]-newAxis[0])
	for ii in range(len(dataBinned)):
		for jj in range(len(data)):
			if newAxis[ii] <= axis[jj] < newAxis[ii] + resolution:
				dataBinned[ii] += data[jj]
				
	return dataBinned

# transforms an axis and data to polar coordinates
def to_polar(data, axis, r0=0):
	r0_pow2 = math.pow(r0,2)
	shape = np.shape(data)
	[y_len,x_len] =shape
	
	f = np.zeros(shape)
	h = zeros(x_len)
	for r in range(0,x_len):
		for phi in range(0,y_len):
			for b in range(0,360):
				U = (r0 + r*sin(b-phi))/r0
				l = math.pow(U,-2)

				for s in range(0,x_len):
					k = r0/(math.pow(r0_pow2 + math.pow(s,2),0.5))   
					f[r,phi] = 0.5*l*k*data[s,b]*h[s]
	return f

# shifts an array (1D) by a given length and fills the empty parts with 0s
def shift(array,length):
	shifted = np.zeros(len(array), dtype = type(array[0]))
	if length <0:
		length = abs(length)
		shifted[length:] = array[:-length]
	elif length > 0:
		shifted[:-length] = array[length:]
	else:
		shifted = array
	return shifted

# calculates the wigner distribution for a 1D complex or real field, returned as a 2D matrix
def wigner(data, axis):
	d_axis = abs(axis[1]-axis[0])
	axis_prime = axis

	d_axis_prime = abs(axis_prime[1]-axis_prime[0])

	axis_ratio = d_axis/d_axis_prime

	
	axis_prime_len = len(axis_prime)
	axis_len = len(axis)
	
	d_axis_prime_fft = 2*math.pi/abs(axis_prime[-1]-axis_prime[0])

	axis_prime_fft = np.linspace(-d_axis_prime_fft*axis_prime_len/2,d_axis_prime_fft*axis_prime_len/2,axis_prime_len)


	E = np.zeros((axis_prime_len,axis_len))*1j
	Econj = np.zeros((axis_prime_len,axis_len))*1j

	offset = axis_prime_len/2

	for ii in range(axis_prime_len):
		ind_shift = int((ii-offset)*axis_ratio/2)

		E[ii,:] = shift(data, -ind_shift)
		Econj[ii,:] = shift(data, ind_shift)

	Econj = np.conj(Econj)

	return np.absolute(np.fft.fft(np.fft.fftshift(E*Econj, axes = 0), axis =0)), axis, axis_prime_fft


def wigner_test(data,axis, contains_CC = True):
	axis_prime = axis
	
	axis_prime_len = len(axis_prime)

	d_axis_prime_fft = 2*math.pi/abs(axis_prime[-1]-axis_prime[0])

	# if the field conatins the complex conjugated part, do an axis from -w to w, otherwise go from 0 to w
	if contains_CC:
		axis_prime_fft = (np.arange(axis_prime_len)-axis_prime_len/2)*d_axis_prime_fft+d_axis_prime_fft/2
	else:
		axis_prime_fft = np.linspace(0,d_axis_prime_fft*axis_prime_len,axis_prime_len, endpoint = False)

	N = len(data)
	N2 = axis_prime_len

	x = np.fft.fftshift((np.arange(N)-N/2)*2*math.pi/(N-1))
	X = (np.arange(N2)-N2/2)


	EX = np.outer(np.ones(N2), np.fft.fft(data))

	Xx = np.outer(X,x)

	EX1 = np.fft.ifft(EX*np.exp(-1j*Xx/2), axis = 1)

	EX2 = np.fft.ifft(EX*np.exp(1j*Xx/2), axis = 1	)

	return np.flipud(np.real(np.fft.fftshift(np.fft.fft(np.fft.ifftshift( EX1*np.conj(EX2),axes = 0), axis = 0), axes =0))), axis, axis_prime_fft



# calculates the frog trace from a 1D field
# data: the 1D complex field
# time: time axis corresponding to data
def FROG_trace(data, time):
	tau = time
	tauLength = len(time)
	# yolo
	dataPadded = data
	timePadded = time
	omegaLength = len(timePadded)
	
	# get the omega spacing after fft
	dOmega = 2*np.pi/abs(timePadded[-1]-timePadded[0])

	# create the omega axis
	omega = np.linspace(0,dOmega*omegaLength,omegaLength, endpoint = False)
	
	# create the signal field (2D)
	esig = np.zeros((tauLength,omegaLength))*1j

	tauLengthHalf = tauLength/2

	for ii in range(tauLength):
		esig[ii,:] = dataPadded*shift(dataPadded, -(ii-tauLengthHalf))

	return np.power(np.absolute(np.fft.fft(np.fft.fftshift(esig, axes =1), axis=1)),2), omega, tau


# calculate the angular energy ratio which is contained in a specific angular range
# angularField: the intensity / field strength of the laser for different angles and spectrum
# axis: the axis corresponding to the field [angle, omega]
# angleMin / angleMax: the upper and lower limit of the angular region
# symmetric: boolean if the field is symmetric and the angular region + pi is also taken into account
def energy_distribution(angularField, axis, angleMin, angleMax, symmetric = False):
	angleAxis = axis[1]
	omegaAxis = axis[0]

	# cut out the spectral region
	ind_low_o = np.argmin(abs(omegaAxis-1.7e15))
	ind_high_o = np.argmin(abs(omegaAxis-1.85e15))

	#cut out the angular region
	ind_low = np.argmin(abs(angleAxis-angleMin))
	ind_high = np.argmin(abs(angleAxis-angleMax))
	
	# sum over the spectral axis
	sum_in_angle = np.sum(angularField[ind_low:ind_high,ind_low_o:ind_high_o])

	# get the full energy / amplitude
	sum_complete = np.sum(angularField[:,ind_low_o:ind_high_o])

	if symmetric:
		ind_high = np.argmin(abs(angleAxis+angleMin))
		ind_low = np.argmin(abs(angleAxis+angleMax))
		symmetric_sum = np.sum(angularField[ind_low:ind_high,ind_low_o:ind_high_o])
		sum_in_angle += symmetric_sum
		sum_outside -= symmetric_sum

	# normalize it to the full energy
	return sum_in_angle/sum_complete*100

# calculates the relativistic corrected critical density for a given wavelength
# intensity in W/cm^2
# wavelength in um
def rel_corr_nc(criticalDensity, intensity,wavelength=1.053):
	return criticalDensity*np.sqrt(1+intensity*wavelength**2/2.74e18)

# smooth data by convolution (moving average)
def smooth(xData,yData, steps):
	NewDimension = np.ones(steps)/steps
	MovingAveragex = np.convolve(xData, NewDimension, mode='valid')
	MovingAveragey = np.convolve(yData, NewDimension, mode='valid')
	return MovingAveragex,MovingAveragey

### calculate bin size from maximum particle energy and energy range, respectively
def calc_binning(E,exponent):
	if max(E)<1 and exponent[1]!=0:
		dE=10**(exponent[1])/1000
	elif exponent[1]==0:
		dE=0.01
	else: 
		dE=10**(exponent[1])/100.
	return dE

### extract the exponent in the basis of 10 from the value (e.g. 2.5e4 -> 2.5, 4) -> needed for calculation of binning
def frexp10(x):	
	exp = int(math.log10(x))
	return x / 10**exp, exp

### do a binning 
# xq: your array containing the query points
# x: your data points
# dx: your bin size
# weight: your weight for each data point (e. g. particle weight)
def do_binning(xq,x,dx,weight):
	z=0
	N=np.zeros(len(xq))
	while z < len(xq):
		k=0
		while k < len(x):
			# if energy in given interval, add weight of particle
			if xq[z]<=x[k]<xq[z]+dx:
				N[z]=N[z]+weight[k]
			k+=1
		z+=1
	Nscaled=N/dx # scale particle numbers to energy binning -> dN/dE
	return Nscaled

### rotates a matrix, not tested
def rot(matrix, rotation_point, angle):
    mat_rot = rotate(matrix,angle) 
    org_center = (np.array(matrix.shape[:2][::-1])-1)/2.
    rot_center = (np.array(mat_rot.shape[:2][::-1])-1)/2.
    org = rotation_point-org_center
    a = np.deg2rad(angle)
    new = np.array([org[0]*np.cos(a) + org[1]*np.sin(a),
            -org[0]*np.sin(a) + org[1]*np.cos(a) ])
    return mat_rot, new+rot_center

### get the values of a matrix along an arbitrary line defined by x0,y0 and x1,y1. The values can be chosen by interpolation (method = 'interpolate') or by choosing the nearest value (method = 'nearest')
def line_values(matrix, axis, p0, p1, pixel_values = False, method = 'interpolate'):
	if not(pixel_values):
		x0, x1 = np.argmin(abs(axis[0]-p0[0])), np.argmin(abs(axis[0]-p1[0]))
		y0, y1 = np.argmin(abs(axis[1]-p0[1])), np.argmin(abs(axis[1]-p1[1]))
	else:
		x0,x1 = p0[0],p1[0]
		y0,y1 = p0[1], p1[1]

	num= (np.sqrt((x1-x0)**2+(y1-y0)**2)).astype(int)
	x, y = np.linspace(x0, x1, num), np.linspace(y0, y1, num)

	# Extract the values along the line and return the corresponding new axis the method can be chosen to interpolate the values along the line, or choose the nearest value close to the line
	if method.lower() == 'interpolate':
		return spi.map_coordinates(matrix, np.vstack((y,x))), np.linspace(0, np.sqrt((p1[0]-p0[0])**2+(p1[1]-p0[1])**2),num)
	elif method.lower() == 'nearest':
		return matrix[y.astype(np.int), x.astype(np.int)], np.linspace(0, np.sqrt((p1[0]-p0[0])**2+(p1[1]-p0[1])**2),num)
	else:
		print('Method used for the funtion line_values not found')

# finds the maximum and minimum points within a grid which corresponds to the line at a specified angle (deg) through the reference point
def find_points(angle, axis, ref_point):
	x, y = axis[0], axis[1]

	#x_ref, y_ref = np.argmin(abs(x-ref_point[0])), np.argmin(abs(y-ref_point[1]))

	x_mesh, y_mesh = np.meshgrid(x-ref_point[0],y-ref_point[1])

	angles = np.arctan(y_mesh/x_mesh)/np.pi*180

	# check on the left side of the box

	right_border = angles[:,-1]
	upper_border = angles[0,:]

	ind_right = np.argmin(abs(right_border-angle))
	ind_upper = np.argmin(abs(upper_border-angle))

	if abs(right_border[ind_right]-angle) < abs(upper_border[ind_upper]-angle):
		p1_x = x[-1]
		p1_y = y[ind_right]
	else:
		p1_x = x[ind_upper]
		p1_y = y[0]

	# check on the right side of the box

	left_border = angles[:,0]
	lower_border = angles[-1,:]

	ind_left = np.argmin(abs(left_border-angle))
	ind_lower = np.argmin(abs(lower_border-angle))

	if abs(left_border[ind_left]-angle) < abs(lower_border[ind_lower]-angle):
		p0_x = x[0]
		p0_y = y[ind_left]
	else:
		p0_x = x[ind_lower]
		p0_y = y[-1]
	
	return [p0_x, p0_y], [p1_x, p1_y]

def poynting_vector(Ex,Ey,Ez, Bx, By, Bz):
	mu0 = 1.25663706212e-6
	
	Sx = Ey*Bz-Ez*By
	Sy = Ez*Bx-Ex*Bz
	Sz = Ex*By-Ey*Bz

	return Sx/mu0,Sy/mu0,Sz/mu0



if __name__=='__main__':
	main()