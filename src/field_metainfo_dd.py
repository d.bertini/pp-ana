import dask.dataframe as dd
import pandas as pd
import numpy as np

class field_metainfo_dd(object):
    """
    A class to encapsulate field metadata and provide functionality for 
    converting it to Dask DataFrames and writing/reading to/from Parquet files.

    Parameters
    ----------
    field_meta_info : openpmd_viewer.FieldMetaInfo
        An instance of the OpenPMD FieldMetaInfo class containing metadata 
        about the field.

    Attributes
    ----------
    axes : dict
        A dictionary indicating the names of the coordinate axes.
    xmin : float
        The minimum value along the x-axis.
    xmax : float
        The maximum value along the x-axis.
    zmin : float
        The minimum value along the z-axis.
    zmax : float
        The maximum value along the z-axis.
    dx : float
        The resolution of the grid along the x-axis.
    dz : float
        The resolution of the grid along the z-axis.
    x : np.ndarray
        A 1D array of x-coordinates of the grid points.
    z : np.ndarray
        A 1D array of z-coordinates of the grid points.
    t : float
        The simulation time of the data.
    iteration : int
        The iteration number of the data.
    field_attrs : dict
        A dictionary containing attributes of the field record.
    component_attrs : dict
        A dictionary containing attributes of the field component record.

    Methods
    -------
    to_dask_dataframes()
        Converts the field metadata into two Dask DataFrames: one for 
        array data and one for single scalar values.
    
    write_to_parquet(file_path)
        Writes the combined DataFrame to a Parquet file at the specified 
        file path.
    
    read_from_parquet(file_path)
        Static method that reads a Parquet file and returns the combined 
        Dask DataFrame.
    """

    def __init__(self, field_meta_info, global_field_shape, local_field_shape):
        # Extract attributes from the OpenPMD FieldMetaInfo instance
        self.axes = field_meta_info.axes
        self.xmin = field_meta_info.xmin
        self.xmax = field_meta_info.xmax
        self.zmin = field_meta_info.zmin
        self.zmax = field_meta_info.zmax
        self.dx = field_meta_info.dx
        self.dz = field_meta_info.dz
        self.x = field_meta_info.x
        self.z = field_meta_info.z
        self.t = field_meta_info.time
        self.iteration = field_meta_info.iteration
        self.field_attrs = field_meta_info.field_attrs
        self.component_attrs = field_meta_info.component_attrs
        self.global_field_shape = global_field_shape
        self.local_field_shape = local_field_shape          
        
    def to_dask_dataframes(self):
        """
        Converts the field metadata into two Dask DataFrames: one for 
        array data and one for single scalar values.

        Returns
        -------
        dask.dataframe.DataFrame
            A combined Dask DataFrame containing both array and scalar 
            metadata.
        """
        #print("Length of z:", len(self.z))

        # Ensure x and z are of the same length
        min_length = min(len(self.x), len(self.z))
        self.x = self.x[:min_length]
        self.z = self.z[:min_length]
        # Extracting attributes for DataFrames
        array_data = {
            'x': self.x,  # 1D array
            'z': self.z,  # 1D array
        }

        # Create a Dask DataFrame for the arrays
        array_df = dd.from_pandas(pd.DataFrame(array_data), npartitions=1) 

        # Extracting single values
        single_value_data = {
            'xmin': [self.xmin],
            'xmax': [self.xmax],
            'zmin': [self.zmin],
            'zmax': [self.zmax],
            'dx':   [self.dx],
            'dz':   [self.dz],
            't':    [self.t],
            'field_attrs': [self.field_attrs],
            'component_attrs': [self.component_attrs],
            'g_shape_x': [self.global_field_shape[0]],
            'g_shape_y': [self.global_field_shape[1]],
            'g_shape_z': [self.global_field_shape[2]],
            'l_shape_x': [self.local_field_shape[0]],
            'l_shape_y': [self.local_field_shape[1]],
            'l_shape_z': [self.local_field_shape[2]]            
        }
        
        # Create a DataFrame for the single values
        single_value_df = dd.from_pandas(pd.DataFrame(single_value_data), npartitions=1)  

        # Align single values with the array DataFrame
        #single_value_df_repeated = single_value_df.map_partitions(lambda df: pd.concat([df]*len(array_df), ignore_index=True))
        # Concatenate the two DataFrames
        #combined_df = dd.concat([array_df, single_value_df_repeated], axis=1)
        #return combined_df
        
        return single_value_df

    def write_to_parquet(self, file_path):
        """
        Writes the combined DataFrame to a Parquet file at the specified 
        file path.

        Parameters
        ----------
        file_path : str
            The path where the Parquet file will be saved.
        """
        combined_df = self.to_dask_dataframes()
        #combined_df.to_parquet(file_path, write_options={'compression': 'snappy'})  
        combined_df.to_parquet(file_path, engine='pyarrow')

    @staticmethod
    def read_from_parquet(file_path):
        """
        Reads a Parquet file and returns the combined Dask DataFrame.

        Parameters
        ----------
        file_path : str
            The path to the Parquet file to be read.

        Returns
        -------
        dask.dataframe.DataFrame
            A Dask DataFrame containing the data read from the Parquet file.
        """
        
        combined_df = dd.read_parquet(file_path)
        return combined_df
