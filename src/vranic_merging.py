

def get_start_ranges(data):

    momentum_start = 0

    x_min = min(data[:,momentum_start])
    x_max = max(data[:,momentum_start])

    x_segment = Segment(x_min, x_max)

    y_min = min(data[:,momentum_start+1])
    y_max = max(data[:,momentum_start+1])

    y_segment = Segment(y_min, y_max)

    z_min = min(data[:,momentum_start+2])
    z_max = max(data[:,momentum_start+2])

    z_segment = Segment(z_min, z_max)

    return x_segment, y_segment, z_segment



def get_position_idx3d(x_patch, y_patch, z_patch, size_y, size_z):
    return (x_patch * size_y + y_patch) * size_z + z_patch


def get_cell_idx(max_coord, min_coord, separator, x_current):
    """ Get name of particles group """
    lenght = max_coord - min_coord
    if lenght == 0:
        return 0
    else:
        return max(0, min(int((x_current - min_coord) * separator / lenght), separator - 1))



def binning_part_pos(data, weights, tolerance_position):

    column_id=0
    x_min = min(data[:,column_id])
    x_max = max(data[:,column_id])
    y_min = min(data[:,column_id + 1])
    y_max = max(data[:,column_id + 1])
    z_min = min(data[:,column_id + 2])
    z_max = max(data[:,column_id + 2])

    num_x_steps = int((x_max - x_min)/ tolerance_position) + 1
    num_y_steps = int((y_max - y_min)/ tolerance_position) + 1
    num_z_steps = int((z_max - z_min)/ tolerance_position) + 1

    numbins = num_x_steps * num_y_steps * num_z_steps

    result_data = [np.zeros((0,data.shape[1])) for i in range(numbins)]
    weight_data = [np.zeros((0)) for i in range(numbins)]

    for i in range(0, data.shape[0]):
        x_idx = get_cell_idx(x_max, x_min, num_x_steps, data[i][column_id])
        y_idx = get_cell_idx(y_max, y_min, num_y_steps, data[i][column_id + 1])
        z_idx = get_cell_idx(z_max, z_min, num_z_steps, data[i][column_id + 2])

        current_idx = get_position_idx3d(x_idx, y_idx, z_idx, num_y_steps, num_z_steps)
        result_data[current_idx] = np.append(result_data[current_idx],data[i,:][None,:],axis=0)
        weight_data[current_idx] = np.append(weight_data[current_idx],weights[i])

    return result_data, weight_data



class Vranic_merging:
    """Implementation of the Vranic et al. merging algorithm. """

    def __init__(self, tolerance_position, tolerance_momentum):
        self.tolerance_momentum = tolerance_momentum
        self.tolerance_position = tolerance_position

    def _run(self, data, weigths):
        data = np.array(data)
        weigths = np.array(weigths)

        data_binned_pos, weights_binned_pos = \
                              part_binning_pos(data, weigths, self.tolerance_position)

        print(data_binned_pos)
        print(weights_binned_pos)        
            
        result_data = np.zeros_like(data)
        result_weight = np.zeros_like(weigths)

        particle_count = 0

        for i in range(len(data_binned_pos)):
            if data_binned_pos[i].shape[0] == 0:
                continue
            x_segment, y_segment, z_segment = get_start_ranges(data_binned_pos[i])

        return result_data, result_weight
    
