#!/bin/bash

#SBATCH --ntasks 1   --cpus-per-task 40 --mem-per-cpu=4G  --partition=main  -o %x-%j.out.log  -e %x-%j.err.log --job-name opmd
#SBATCH hetjob
#SBATCH --ntasks 100 --cpus-per-task 10  --mem-per-cpu=4G  --partition=main   -o %x-%j.out.log -e %x-%j.err.log

#Containers
export CONT=/lustre/rz/dbertini/containers/orion/adapt/rlx8_ompi_ucx_gcc13_py312.sif

export WDIR=/lustre/rz/dbertini/gpu/warpx/mpi_ana/analysis
export OMPI_MCA_io=romio341
export APPTAINER_BINDPATH=/lustre/rz/dbertini/,/cvmfs

# UCX settings
#export UCX_LOG_LEVEL=info
export UCX_IB_RCACHE_MAX_REGIONS=1000
export UCX_RC_MLX5_TX_NUM_GET_BYTES=256k
export UCX_RC_MLX5_MAX_GET_ZCOPY=32k

# MPI4PY settings
export MPI4PY_RC_THREAD_LEVEL=single

# new apptainer
export APPTAINER_CONFIGDIR=/lustre/rz/dbertini/gpu/warpx/mpi_ana/analysis
export APPTAINER_SHARENS=true
export APPTAINER_MESSAGELEVEL=-3

EXE=python
INPUT=$WDIR/opmd_analyser.py

srun  --export=ALL  --het-group=0 singularity exec ${CONT} ${EXE} ${INPUT} :  --het-group=1 singularity exec ${CONT} ${EXE} ${INPUT}
