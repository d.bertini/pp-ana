#!/bin/bash

# Container definition 
export CONT=/lustre/rz/dbertini/containers/dev/cpu/rlx8_ompi_ucx_slurm.sif

# UCX + MPI settings
#export UCX_LOG_LEVEL=info
#export UCX_RCACHE_MAX_REGIONS=1000
#export UCX_RC_MLX5_TX_NUM_GET_BYTES=256k
#export UCX_RC_MLX5_MAX_GET_ZCOPY=32k
#export OMPI_MCA_io=romio341

# Apptainer instances
export APPTAINER_BINDPATH=/lustre/rz/dbertini/,/cvmfs
export APPTAINER_CONFIGDIR=$SLURM_SUBMIT_DIR
export APPTAINER_SHARENS=true
export APPTAINER_MESSAGELEVEL=-3

# Main script
INPUT=$APPTAINER_CONFIGDIR/run-python.sh
srun  --export=ALL --cpu-bind=cores singularity  exec  ${CONT} ${INPUT}
