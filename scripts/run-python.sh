#!/bin/bash

# Matplotlib cache?
export MPLCONFIGDIR=$SLURM_SUBMIT_DIR

export WDIR=/lustre/rz/dbertini/pp-ana/analysis
export OPMD_DIR=/lustre/rz/dbertini2/gpu/warpx/scripts/diags/lwfa_3d/
export OUTPUT_DIR=/lustre/rz/dbertini/pp-ana/data
export PLOTS_DIR=/lustre/rz/dbertini/pp-ana/plots
EXE=python
SPECIES=electrons
OPMD_FILE=openpmd_020000.bp


# Analyzer ( Read + PLots )
# Analyse only one file
#${EXE} ${WDIR}/opmd_analyser.py -d ${OPMD_DIR} -f ${OPMD_FILE} -s ${SPECIES} -o ${OUTPUT_DIR}
# Analyse a full directory
#${EXE} ${WDIR}/opmd_analyser.py -d ${OPMD_DIR}  -s ${SPECIES} -o ${OUTPUT_DIR}

# Filter ( Read openPMD -> selection -> parquet )
#${EXE} ${WDIR}/opmd_filter.py -d ${OPMD_DIR} -f ${OPMD_FILE} -s ${SPECIES} -o ${OUTPUT_DIR}

# PQ reader
${EXE} ${WDIR}/opmd_pq_reader.py -d ${OUTPUT_DIR} -f ${OPMD_FILE} -s ${SPECIES} -o ${PLOTS_DIR} -a 'full'







