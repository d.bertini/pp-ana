# Run the openPMD reader 

sbatch  --nodes 4  --ntasks-per-node 32 --cpus-per-task 2 --mem-per-cpu 4G  --no-requeue --job-name opmd  --mail-type ALL --mail-user d.bertini@gsi.de --partition debug --constraint=amd,epyc --time 0-00:30:00  -o %j.out.log -e %j.err.log   ./run-file.sh

#sbatch --ntasks 300 --cpus-per-task 1 --mem-per-cpu 8G  --no-requeue --job-name opmd  --mail-type ALL --mail-user d.bertini@gsi.de --partition high_mem --constraint=amd,epyc --time 0-08:00:00  -o %j.out.log -e %j.err.log   ./run-file.sh
