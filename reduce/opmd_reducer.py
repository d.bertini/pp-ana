import os
import sys
from sys import exit
import numpy as np
import pandas as pd
import random

import scipy.constants as const

from mpi4py import MPI
import openpmd_api as io

from src.openpmd_reader  import opmd_reader

# Declare global MPI setup
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
         
if __name__ == "__main__":
    # Communicator as global?
    comm = MPI.COMM_WORLD
    opmd_r = opmd_reader("/lustre/rz/dbertini/gpu/warpx/scripts/diags/lwfa_3d/", "openpmd_%T.bp")
    series = opmd_r.get_series() 

    # Get iteration for step=istep 
    istep = 20000
    i = series.iterations[istep]

    # Get Meta Data Infos
    if 0 == comm.rank:
        print("Read a series in parallel with {} MPI ranks".format(
            comm.size))
    
        print("Read a Series with openPMD standard version %s" %
              series.openPMD)
        
        print("Time series contains {0} iterations:".format(len(series.iterations)))
        for it in series.iterations:
            print("\t {0}".format(it))
            print("")
            
        print(opmd_r.get_timestep_values())
        print(opmd_r.get_fields())
        print(opmd_r.get_species())
        print(opmd_r.get_particles_records())

                
    # Read particles information in pieces  
    n_part  = 0
    d_npart = 0

    n_part = opmd_r.get_n_part(i,"electrons")    
    d_npart = n_part // comm.size
    
    start = comm.rank * d_npart
    if comm.rank < (comm.size - 1 ):
        end = d_npart
    else:
        end = n_part - (comm.size-1) * d_npart

    # Get kinematics info    
    part_pos = opmd_r.get_part_list(i,"electrons","position", start, end)         
    part_p = opmd_r.get_part_list(i,"electrons","momentum", start, end)
    part_w = opmd_r.get_part_list(i,"electrons","weighting", start, end)
    
    # Close here the input  
    opmd_r.get_series().close()
        
    # Get Max Min from x and y
    x_loc = part_pos[...,0]
    y_loc = part_pos[...,2]    
    px_loc = part_p[...,0]/const.m_e
    py_loc = part_p[...,2]/const.m_e        
    w_loc = part_pos[...,0] 

    # Move to contiguous
    x_loc = np.transpose(x_loc)
    y_loc = np.transpose(y_loc)
    px_loc = np.transpose(px_loc)
    py_loc = np.transpose(py_loc)
    w_loc = np.transpose(w_loc)
    
    x_loc = np.ascontiguousarray(x_loc)
    y_loc = np.ascontiguousarray(y_loc)
    px_loc = np.ascontiguousarray(px_loc)
    py_loc = np.ascontiguousarray(py_loc)    
    w_loc = np.ascontiguousarray(w_loc)
    
    print(" Rank {} non-selected len_x_loc {} len_y_loc {} "\
          .format(comm.rank, len(x_loc), len(y_loc)))            

    # Now do some data selection/reduction ( later on function)
    uz_indexes = np.where(py_loc>25)[0]
    x_loc = x_loc[uz_indexes]
    y_loc = y_loc[uz_indexes]    
    px_loc = px_loc[uz_indexes]
    py_loc = py_loc[uz_indexes]
    w_loc = w_loc[uz_indexes]
    

    print(" Rank {} selected len_x_loc {} len_y_loc {} "\
          .format(comm.rank, len(x_loc), len(y_loc)))            


    # Now try to write back the reduced
    g_ntracks = np.zeros(size)
    offsets= np.zeros(size)
    
    # Store the reduced size
    l_ntracks = len(x_loc)

    a_size = 1
    senddata = np.arange(a_size,dtype=np.int32)
    senddata[0] = len(x_loc)
    recvdata = np.arange(size*a_size,dtype=np.int32)
    comm.Allgather(senddata,recvdata)
    #print ('on task {} after gather {} ntotal_tracks {} '.format(rank,recvdata, recvdata.sum()))
    
    # open file for writing
    series = io.Series(
        "/lustre/rz/dbertini/gpu/warpx/mpi_ana/scripts/data/reduced_xypxpyw.bp",
        io.Access.create,
        comm
    )
    
    series.iterations[istep].open()
    electrons = series.iterations[istep].particles["electrons"]
    e_x  = electrons["position"]["x"]
    e_z  = electrons["position"]["z"]
    e_px = electrons["momentum"]["x"]
    e_pz = electrons["momentum"]["z"]
    e_w  = electrons["weigthing"][io.Mesh_Record_Component.SCALAR]
   
    # Dataset definition
    global_extent = [recvdata.sum()]
    dataset = io.Dataset(x_loc.dtype, global_extent)

    # Offset, extent
    offset=0                
    for idx, x in enumerate(recvdata):
        if ( idx < rank ):
            offset+=recvdata[idx]
    # (x, z )
    e_x.reset_dataset(dataset)
    e_x.store_chunk(x_loc, [offset,], [recvdata[rank],])
    e_z.reset_dataset(dataset)
    e_z.store_chunk(y_loc, [offset,], [recvdata[rank],])

    # px
    e_px.reset_dataset(dataset)
    e_px.store_chunk(px_loc, [offset,], [recvdata[rank],])
    e_pz.reset_dataset(dataset)
    e_pz.store_chunk(py_loc, [offset,], [recvdata[rank],])    

    # w
    e_w.reset_dataset(dataset)
    e_w.store_chunk(w_loc, [offset,], [recvdata[rank],])
    
    series.iterations[istep].close()
    series.close()
    
    if 0 == comm.rank:
        print("end of the computation")
       
        



