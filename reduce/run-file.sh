#!/bin/bash

# Containers
export CONT=/lustre/rz/dbertini/containers/orion/adapt/rlx8_ompi_ucx_dask_mt.sif

export WDIR=/lustre/rz/dbertini/gpu/warpx/mpi_ana
export OMPI_MCA_io=romio341
export APPTAINER_BINDPATH=/lustre/rz/dbertini/,/cvmfs

# UCX settings
#export UCX_LOG_LEVEL=info
export UCX_IB_RCACHE_MAX_REGIONS=1000
export UCX_RC_MLX5_TX_NUM_GET_BYTES=256k
export UCX_RC_MLX5_MAX_GET_ZCOPY=32k

EXE=python
INPUT=$WDIR/reduce/opmd_reducer.py

srun --export=ALL --cpu-bind=cores singularity  exec  ${CONT} ${EXE} ${INPUT}
