# Run the openPMD reducer 
sbatch  --ntasks 200 --no-requeue --job-name opmd --mem-per-cpu 4000 --mail-type ALL --mail-user d.bertini@gsi.de --partition main  --time 0-08:00:00  -o %j.out.log -e %j.err.log -D ./data  ./run-file.sh
